package com.mcconaughey.matthew.everest.activities;

import com.mcconaughey.matthew.everest.R;
import com.stephentuso.welcome.WelcomeScreenBuilder;
import com.stephentuso.welcome.ui.WelcomeActivity;
import com.stephentuso.welcome.util.WelcomeScreenConfiguration;

/**
 * Created by alexander on 23.05.16.
 */

public class OnBoardingScreen extends WelcomeActivity {

    @Override
    protected WelcomeScreenConfiguration configuration() {
        return new WelcomeScreenBuilder(this)
                .theme(R.style.WelcomeScreenTheme_Light_Mine)
                .defaultTitleTypefacePath("fonts/SketchRockwell-Bold.ttf")
                .defaultHeaderTypefacePath("fonts/SketchRockwell-Bold.ttf")
                .defaultDescriptionTypefacePath("fonts/SketchRockwell-Bold")
                .titlePage(R.drawable.logo1, "EVEREST", R.color.colorPrimary)
                .basicPage(R.drawable.logo1, "Легок в использовании", "Создавайте себе цели", R.color.colorAccent)
                .basicPage(R.drawable.logo1, "Многофункционален", "Разбивайте цели на подзадачи", R.color.colorPrimaryDark)
                .basicPage(R.drawable.logo1, "Следите за жизнью друзей", "Находите новых людей и познавайте мир!", R.color.colorPrimary)
                .basicPage(R.drawable.logo1, "Добро пожаловать!", "", R.color.colorAccent)
                .swipeToDismiss(true)
                .exitAnimation(android.R.anim.fade_out)
                .build();
    }

    public static String welcomeKey() {
        return "WelcomeScreen";
    }

}