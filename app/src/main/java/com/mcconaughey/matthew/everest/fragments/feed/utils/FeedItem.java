package com.mcconaughey.matthew.everest.fragments.feed.utils;

import android.graphics.drawable.Drawable;

/**
 * Created by alexander on 13.03.16.
 */

public class FeedItem {
    public String userName;
    public String date;
    public String shortDescription;
    public String rating;
    public String rated;
    public String taskID;
    public String description;
    public Drawable userAvatar;
    public Drawable taskImage;

    public FeedItem(String userName, String date, String shortDescription, String rating,
                    String taskID, String description, Drawable userAvatar, Drawable taskImage, String rated) {
        this.userName = userName;
        this.date = date;
        this.shortDescription = shortDescription;
        this.rating = rating;
        this.taskID = taskID;
        this.description = description;
        this.taskImage = taskImage;
        this.userAvatar = userAvatar;
        this.rated = rated;
    }

    public String getItemId() {
        return this.taskID;
    }

    public void setRated( String rated) {
        this.rated = rated;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
