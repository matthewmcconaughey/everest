package com.mcconaughey.matthew.everest.fragments.feed;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;
import com.mcconaughey.matthew.everest.events.CreateTaskEvent;
import com.mcconaughey.matthew.everest.events.FeedReceivedAndSavedEvent;
import com.mcconaughey.matthew.everest.events.RateTaskEvent;
import com.mcconaughey.matthew.everest.fragments.addtask.data.TaskEvent;
import com.mcconaughey.matthew.everest.fragments.feed.utils.FeedAdapter;
import com.mcconaughey.matthew.everest.fragments.feed.utils.FeedItem;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;
import com.mcconaughey.matthew.everest.utils.SubscribesGetter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class FeedFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private SwipeRefreshLayout swipeRefreshLayout;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feed, container, false);

    }

    RecyclerView rv;
    List<FeedItem> feedItemList;
    FeedAdapter adapter;
    FeedItem feedItem;


    @Override
    public void onActivityCreated(Bundle savedInstanceSaved) {
        super.onActivityCreated(savedInstanceSaved);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Лента");

        initPullToRefreshObserver();

        rv = (RecyclerView) getView().findViewById(R.id.feed_recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        assert rv != null;
        rv.setLayoutManager(llm);
        rv.setItemAnimator(new SlideInUpAnimator());

        feedItemList = new ArrayList<>();


//        //Надо переделать вызов драваблов тут, в соответствии с min sdk api level 14
//        for (int i = 0; i < 15; i++) {
//            feedItemList.add(new FeedItem("Мэттью Макконахи", "19 февраля 23:00", "Wanna go straight outta Komp" +
//                    "ton aka Interstallar kek", "147", "0", "0", this.getResources().getDrawable(R.drawable.foo),
//                    this.getResources().getDrawable(R.drawable.buz)));
//        }

        adapter = new FeedAdapter(feedItemList, getActivity());
        rv.setAdapter(adapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (feedItem != null) {
            feedItemList.add(0, feedItem);
            adapter.notifyItemChanged(0);
        }

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                MainActivity.FEED_LOADER, null, ((MainActivity) getActivity()));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        ((AppCompatActivity) getActivity()).getSupportLoaderManager().destroyLoader(
                MainActivity.FEED_LOADER);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showFloatingActionButton();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initPullToRefreshObserver(){
        swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.feedSwipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO : Саня, я не стал тут ебаться с проверкой подключения, ты в этом про, доделай
                ArrayList<String> subscribes = SubscribesGetter.getSubscribes(getActivity().getContentResolver());
                MyProfileGetter.Profile profile = MyProfileGetter.getMyProfile(
                        getActivity().getContentResolver());
                subscribes.add(profile.serverID);
                ServiceHelper.getFeed(getActivity(), profile.accessToken, subscribes);
            }
        });
    }

    @Subscribe
    public void onTaskEvent(TaskEvent event){
//        feedItem = new FeedItem("Мэттью Макконахи", event.time, event.description, "147", this.getResources().getDrawable(R.drawable.foo),
//                this.getResources().getDrawable(R.drawable.buz));
    }

    @Subscribe
    public void onRateTaskEvent(final RateTaskEvent event) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < feedItemList.size(); i++) {
                    if (Objects.equals(feedItemList.get(i).getItemId(), event.itemId)) {
                        if (event.resp.equals("1")) {
                            feedItemList.get(i).setRated("1");
                        } else {
                            feedItemList.get(i).setRated("0");
                        }
                        feedItemList.get(i).setRating(event.rating);
                        adapter.notifyItemChanged(i);
                    }
                }
            }
        });

    }

    @Subscribe
    public void onFeedReceivedAndSavedEvent(FeedReceivedAndSavedEvent event) {
        feedItemList.clear();
        if (event.feed.moveToFirst()) {
            FeedItem item = createFeedItem(event);
            feedItemList.add(item);
            while (event.feed.moveToNext()) {
                item = createFeedItem(event);
                feedItemList.add(item);
            }
        }
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    private FeedItem createFeedItem(FeedReceivedAndSavedEvent event) {
        return new FeedItem(
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_AUTHOR_NAME)),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_CREATION_TIME)),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_SHORT_NAME)),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_RATING)),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_TASK_ID)),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_DESCRIPTION)),
                this.getResources().getDrawable(R.drawable.foo),
                this.getResources().getDrawable(R.drawable.buz),
                event.feed.getString(event.feed.getColumnIndex(TaskContract.Tasks.COLUMN_NAME_RATED))
        );
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
