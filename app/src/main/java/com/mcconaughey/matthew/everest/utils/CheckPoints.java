package com.mcconaughey.matthew.everest.utils;

import android.os.Parcel;
import android.os.Parcelable;

import com.mcconaughey.matthew.everest.fragments.addtask.data.CheckPointsDataProvider;

import java.util.ArrayList;

/**
 * Created by dmitri on 28.05.16.
 */
public class CheckPoints implements Parcelable {
    public ArrayList<CheckPoint> checkPoints;

    public static final Parcelable.Creator<CheckPoints> CREATOR = new Parcelable.Creator<CheckPoints>() {

        @Override
        public CheckPoints createFromParcel(Parcel source) {
            return new CheckPoints(source);
        }

        @Override
        public CheckPoints[] newArray(int size) {
            return new CheckPoints[size];
        }
    };

    public CheckPoints(Parcel in) {
        checkPoints = in.createTypedArrayList(CheckPoint.CREATOR);
    }

    public CheckPoints(ArrayList<CheckPointsDataProvider.CheckPointData> checkPoints) {
        this.checkPoints = new ArrayList<>();
        for (CheckPointsDataProvider.CheckPointData checkPointData: checkPoints) {
            this.checkPoints.add(new CheckPoint(null, checkPointData.id, checkPointData.text,
                    checkPointData.state ? 1 : 0));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(checkPoints);
    }
}
