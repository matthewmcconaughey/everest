package com.mcconaughey.matthew.everest.network.services;

import android.app.IntentService;
import android.content.Intent;

import com.mcconaughey.matthew.everest.events.PeopleFoundedEvent;
import com.mcconaughey.matthew.everest.events.SubscribeStateEvent;
import com.mcconaughey.matthew.everest.network.ProfileProcessor;
import com.mcconaughey.matthew.everest.network.REST;

import org.greenrobot.eventbus.EventBus;

public class ProfileIntentService extends IntentService {

    public final static String ACTION_GET_SUBSCRIBES = "action.GET_SUBSCRIBES";
    public final static String ACTION_GET_PROFILE = "action.GET_PROFILE";
    public final static String ACTION_UPDATE_PROFILE = "action.UPDATE_PROFILE";
    public final static String ACTION_FIND_PEOPLE = "action.FIND_PEOPLE";
    public final static String ACTION_SUBSCRIBE = "action.SUBSCRIBE";
    public final static String ACTION_UNSUBSCRIBE = "action.UNSUBSCRIBE";

    public final static String EXTRA_ACCESS_TOKEN = "extra.ACCESS_TOKEN";
    public final static String EXTRA_SERVER_ID = "extra.SERVER_ID";
    public final static String EXTRA_NOT_MY_SERVER_ID = "extra.NOT_MY_SERVER_ID";
    public final static String EXTRA_FIELD_TO_UPDATE = "extra.FIELD_TO_UPDATE";
    public final static String EXTRA_VALUE = "extra.VALUE";
    public final static String EXTRA_QUERY = "extra.QUERY";
    public final static String EXTRA_MODE = "extra.MODE";


    public ProfileIntentService() {
        super("ProfileIntentService");


    }

    public ProfileIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_GET_SUBSCRIBES:
                    handleGetSubscribesAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_SERVER_ID), intent.getIntExtra(EXTRA_MODE, 0));
                    break;
                case ACTION_GET_PROFILE:
                    handleGetProfileAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_SERVER_ID));
                    break;
                case ACTION_UPDATE_PROFILE:
                    handleUpdateProfileAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_FIELD_TO_UPDATE),
                            intent.getStringExtra(EXTRA_VALUE));
                    break;
                case ACTION_FIND_PEOPLE:
                    handleFindPeopleAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_QUERY));
                case ACTION_SUBSCRIBE:
                    handleSubscribeAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_SERVER_ID), intent.getStringExtra(EXTRA_NOT_MY_SERVER_ID));
                    break;
                case ACTION_UNSUBSCRIBE:
                    handleUnsubscribeAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_SERVER_ID), intent.getStringExtra(EXTRA_NOT_MY_SERVER_ID));
                    break;
            }
        }
    }

    private void handleGetSubscribesAction(String accessToken, String serverID, int mode) {
        if (accessToken != null && serverID != null)
            ProfileProcessor.processGetSubscribes(getContentResolver(), accessToken, serverID, mode);
    }

    private void handleGetProfileAction(String accessToken, String serverID) {
        if (accessToken != null && serverID != null)
            ProfileProcessor.processProfile(getContentResolver(), accessToken, serverID);
    }

    private void handleUpdateProfileAction(String accessToken, String field, String value) {
        if (accessToken != null && field != null && value != null)
            ProfileProcessor.processUpdateProfile(getContentResolver(), accessToken, field, value);
    }

    private void handleFindPeopleAction(String accessToken, String query) {
        REST.Profile[] people = ProfileProcessor.processFindPeople(getContentResolver(), accessToken, query);
        EventBus.getDefault().post(new PeopleFoundedEvent(people));
    }

    private void handleSubscribeAction(String accessToken, String myServerID, String notMyServerID) {
        // ???????? очень странный баг. в runOnUI findPeople выполняется этот код
        if (accessToken != null && myServerID != null && notMyServerID != null)
            EventBus.getDefault().post(new SubscribeStateEvent(ProfileProcessor.processSubscribe(getContentResolver(),
                accessToken, myServerID, notMyServerID)));
    }

    private void handleUnsubscribeAction(String accessToken, String myServerID, String notMyServerID) {
        // ????????
        if (accessToken != null && myServerID != null && notMyServerID != null)
            EventBus.getDefault().post(new SubscribeStateEvent(!ProfileProcessor.processUnsubscribe(getContentResolver(),
                accessToken, myServerID, notMyServerID)));

    }
}
