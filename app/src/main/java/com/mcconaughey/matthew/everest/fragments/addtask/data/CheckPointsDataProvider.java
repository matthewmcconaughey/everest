/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.mcconaughey.matthew.everest.fragments.addtask.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.mcconaughey.matthew.everest.utils.CheckPoint;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


// TODO: replace mdata with data
public class CheckPointsDataProvider extends AbstractDataProvider {
//    private List<CheckPointData> mData;
    private CheckPointData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    private ArrayList<CheckPointData> data;

    final int swipeReaction = RecyclerViewSwipeManager.REACTION_CAN_SWIPE_UP | RecyclerViewSwipeManager.REACTION_CAN_SWIPE_DOWN;

    public CheckPointsDataProvider() {
        this.data = new ArrayList<>();
    }

    public void addItem(String description) {
        this.data.add(new CheckPointData(data.size(), 0, false, description, swipeReaction));
    }

    public ArrayList<CheckPointData> getCheckPoints() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return data.get(index);
    }

    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < data.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = data.size();
            }

            data.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final CheckPointData item = data.remove(fromPosition);

        data.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final CheckPointData removedItem = data.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class CheckPointData extends Data implements Parcelable {

        public String taskID;
        public final long id;
        public boolean state;
        public String text;
        private final int viewType;
        private boolean pinned;

        CheckPointData(long id, int viewType, boolean state, String text, int swipeReaction) {
            this.id = id;
            this.viewType = viewType;
            this.state = state;
            this.text = text;
        }

        protected CheckPointData(Parcel in) {
            String[] data = new String[3];
            in.readStringArray(data);
            id = Long.valueOf(data[0]);
            text = data[1];
            state = Boolean.valueOf(data[2]);
            viewType = 0;
        }

        public static final Creator<CheckPointData> CREATOR = new Creator<CheckPointData>() {
            @Override
            public CheckPointData createFromParcel(Parcel in) {
                return new CheckPointData(in);
            }

            @Override
            public CheckPointData[] newArray(int size) {
                return new CheckPointData[size];
            }
        };

        private static String makeText(long id, String text, int swipeReaction) {
            final StringBuilder sb = new StringBuilder();

            sb.append(id);
            sb.append(" - ");
            sb.append(text);

            return sb.toString();
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return viewType;
        }

        @Override
        public long getId() {
            return id;
        }

        @Override
        public String toString() {
            return text;
        }

        @Override
        public String getText() {
            return text;
        }

        @Override
        public boolean getState() {
            return state;
        }

        @Override
        public boolean isPinned() {
            return pinned;
        }

        @Override
        public void setText(String text) {
            this.text = text;
        }

        @Override
        public void setPinned(boolean pinned) {
            this.pinned = pinned;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringArray(new String[] {String.valueOf(id), String.valueOf(text),
                    String.valueOf(state)});
        }
    }
}
