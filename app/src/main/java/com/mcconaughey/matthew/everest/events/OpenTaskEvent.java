package com.mcconaughey.matthew.everest.events;

/**
 * Created by dmitri on 29.05.16.
 */
public class OpenTaskEvent {
    public String taskID;
    public String userName;
    public String shortDescription;
    public String description;
    public String date;
    public String rating;

    public OpenTaskEvent(String taskID, String userName, String shortDescription,
                         String description, String date, String rating) {
        this.taskID = taskID;
        this.userName = userName;
        this.shortDescription = shortDescription;
        this.description = description;
        this.date = date;
        this.rating = rating;
    }
}
