package com.mcconaughey.matthew.everest.fragments.sign;

import android.app.Fragment;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.activities.SignActivity;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.SignInEvent;
import com.mcconaughey.matthew.everest.fragments.feed.utils.FeedAdapter;
import com.mcconaughey.matthew.everest.fragments.feed.utils.FeedItem;
import com.mcconaughey.matthew.everest.network.ServiceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class SignInFragment extends Fragment {

    public SignInFragment() { }

    FrameLayout signInContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View signUp = getActivity().findViewById(R.id.link_signup);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 07.04.16 Попросить активити переключить, а не жестко самому
                ((SignActivity)getActivity()).switchFragments("SignUp");
            }
        });

        signInContainer = (FrameLayout) getView().findViewById(R.id.sign_in_container);

        setSignInOnClickListener();
    }


    public void setSignInOnClickListener() {
        final View view = getView();
        Button loginBtn = (Button) view.findViewById(R.id.btn_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SignActivity.hideSoftKeyboard(getActivity());

                String email = ((EditText) view.findViewById(R.id.input_email))
                        .getText().toString();
                String password = ((EditText) view.findViewById(R.id.input_password))
                        .getText().toString();

                if (!(email.isEmpty() && password.isEmpty())){

                    ServiceHelper.signIn(getActivity(), email, password);

                } else {
                    Snackbar snackbar = Snackbar.make(signInContainer, "Заполните все поля!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);
                    snackbar.show();
                }
            }
        });

    }

}
