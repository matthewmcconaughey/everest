package com.mcconaughey.matthew.everest.fragments.friends;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.GetFriendsEvent;
import com.mcconaughey.matthew.everest.events.OpenTaskEvent;
import com.mcconaughey.matthew.everest.fragments.friends.utils.FriendsAdapter;
import com.mcconaughey.matthew.everest.fragments.friends.utils.Sections;
import com.mcconaughey.matthew.everest.fragments.friends.utils.SimpleSectionedRecyclerViewAdapter;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class FriendsFragment extends Fragment {

    public int mode; // что отображается. подписки или подписчики

    FriendsAdapter adapter;

    RecyclerView recyclerView;
    LinearLayout emptyMsg;

    int loaderID;
    Uri contentURI;
    String[] projection;

    public static final int SUBSCRIBES = 0;
    public static final int SUBSCRIBERS = 1;

    LinearLayout headerLayout;

    private SwipeRefreshLayout swipeContainer;


    public FriendsFragment() {
        // Required empty public constructor
    }

    public void initDbVariables(int mode) {
        switch (mode) {
            case SUBSCRIBES:
                loaderID = MainActivity.MY_SUBSCRIBES_LOADER;
                contentURI = ProfileContract.MySubscribes.CONTENT_URI;
                projection = ProfileContract.MySubscribes.DEFAULT_PROJECTION;
                break;
            case SUBSCRIBERS:
                loaderID = MainActivity.MY_SUBSCRIBERS_LOADER;
                contentURI = ProfileContract.MySubscribers.CONTENT_URI;
                projection = ProfileContract.MySubscribers.DEFAULT_PROJECTION;
                break;
            default:
                break;
        }
        this.mode = mode;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friends, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceSaved) {
        super.onActivityCreated(savedInstanceSaved);

        recyclerView = (RecyclerView) getView().findViewById(R.id.friendsRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        assert recyclerView != null;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        initPullToRefreshObserver();

        emptyMsg = (LinearLayout) getView().findViewById(R.id.empty_msg);

    }

    @Override
    public void onResume() {
        super.onResume();

        // TODO: здесь сначала идем в базу за списком друзей
        // TODO: отрисовываем что есть (даже если ниче нет - не беда)
        // TODO: вне зависимости от того, есть что-то в базе или нет, спрашиваем сервер

        EventBus.getDefault().register(this);

        ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                loaderID, null, ((MainActivity) getActivity()));
        Cursor cursor = getActivity().getContentResolver()
                .query(contentURI, projection, null, null, null);
        EventBus.getDefault().post(new GetFriendsEvent(cursor));

        // TODO: нахуя тут лоадер моего профиля? мб надо убрать
        ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                MainActivity.MY_PROFILE_LOADER, null, ((MainActivity) getActivity()));

        cursor = getActivity().getContentResolver().query(
                ProfileContract.MyProfile.CONTENT_URI, ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
        if (cursor.moveToLast()) {
            MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getActivity().getContentResolver());
            ServiceHelper.getSubscribes(getActivity(), myProfile.accessToken,
                    myProfile.serverID, loaderID);
        }
    }

    public void reload() {
        if (adapter != null){
            adapter.clearList();
        }

        EventBus.getDefault().unregister(this);
        ((AppCompatActivity) getActivity()).getSupportLoaderManager().destroyLoader(loaderID);
        EventBus.getDefault().register(this);

        ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                loaderID, null, ((MainActivity) getActivity()));
        Cursor cursor = getActivity().getContentResolver()
                .query(contentURI, projection, null, null, null);
        EventBus.getDefault().post(new GetFriendsEvent(cursor));

        ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                MainActivity.MY_PROFILE_LOADER, null, ((MainActivity) getActivity()));

        cursor = getActivity().getContentResolver().query(
                ProfileContract.MyProfile.CONTENT_URI, ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
        if (cursor.moveToLast()) {
            MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getActivity().getContentResolver());
            ServiceHelper.getSubscribes(getActivity(), myProfile.accessToken,
                    myProfile.serverID, loaderID);
        }


    }

    public void initPullToRefreshObserver(){
        swipeContainer = (SwipeRefreshLayout) getActivity().findViewById(R.id.friends_refresh_layout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Objects.equals(((MainActivity) getActivity()).getConnectionState(), "NO_CONNECTION")){
                    ((AppCompatActivity) getActivity()).getSupportLoaderManager().initLoader(
                            loaderID, null, ((MainActivity) getActivity()));
                    Cursor cursor = getActivity().getContentResolver()
                            .query(contentURI, projection, null, null, null);

                    EventBus.getDefault().post(new GetFriendsEvent(cursor));
                    cursor = getActivity().getContentResolver().query(
                            ProfileContract.MyProfile.CONTENT_URI, ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
                    if (cursor.moveToLast()) {
                        ServiceHelper.getSubscribes(getActivity(), cursor.getString(cursor.getColumnIndex("accessToken")),
                                cursor.getString(cursor.getColumnIndex("serverID")), loaderID);
                    }

                } else {
                    swipeContainer.setRefreshing(false);
                    Snackbar snackbar = Snackbar.make(((MainActivity) getActivity()).coordinatorLayout,
                            "Проверьте соединение с интернетом", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        ((AppCompatActivity) getActivity()).getSupportLoaderManager().destroyLoader(loaderID);
    }

    @Subscribe
    public void onGetFriendsEvent(GetFriendsEvent event) {

        ((MainActivity)getActivity()).getProgressBar().setVisibility(View.GONE);

        ArrayList<FriendsAdapter.FriendItem> list = new ArrayList<>();
        event.friends.moveToFirst();

        if (event.friends.getCount() != 0) {
            do {
//            list.add(event.people.getString(event.people.getColumnIndex("lastName")) + " "
//                    + event.people.getString(event.people.getColumnIndex("name")));
                list.add(new FriendsAdapter.FriendItem(
                        event.friends.getString(event.friends.getColumnIndex("lastName")) + " "
                                + event.friends.getString(event.friends.getColumnIndex("name")),
                        event.friends.getString(event.friends.getColumnIndex("serverID"))));
            } while (event.friends.moveToNext());

        }

        if (!list.isEmpty()) {

            adapter = new FriendsAdapter(getActivity());
            this.adapter.addValues(list);
            this.adapter.notifyDataSetChanged();


            List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

            Sections secs = new Sections(adapter.getList());
            int position = 0;
            for (int i = 0; i < secs.getSize(); i++) {
                sections.add(new SimpleSectionedRecyclerViewAdapter.Section(position, String.valueOf(secs.getCharacterOfSection(i))));
                position += secs.getCapacityOfSection(i);
            }

            SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
            SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                    SimpleSectionedRecyclerViewAdapter(getActivity(), R.layout.section, R.id.section_text, adapter);
            mSectionedAdapter.setSections(sections.toArray(dummy));

            recyclerView.setAdapter(mSectionedAdapter);

        } else {

        }
        swipeContainer.setClickable(true);

        if (swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);

    }

    @Subscribe(sticky = true)
    public void onOpenTaskEvent(OpenTaskEvent event) {
        event.rating.isEmpty();
        event.rating = "1";
    }
}
