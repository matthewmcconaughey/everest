package com.mcconaughey.matthew.everest.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;
import com.mcconaughey.matthew.everest.events.CheckpointsLoaded;
import com.mcconaughey.matthew.everest.events.OpenTaskEvent;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.AccessTokenGetter;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;


public class TaskFragment extends Fragment {

    String taskID;
    TextView userNameTextView;
    TextView dateTextView;
    TextView shortDescriptionTextView;
    TextView descriptionTextView;
    TextView ratingTextView;
    LinearLayout checkpointsLinearLayout;
    ArrayList<CheckBox> checkpointsCheckBoxes;

    boolean mode;

    public TaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        userNameTextView = (TextView) getView().findViewById(R.id.userNameTextView);
        dateTextView = (TextView) getView().findViewById(R.id.dateTextView);
        shortDescriptionTextView = (TextView) getView().findViewById(R.id.shortDescriptionTextView);
        descriptionTextView = (TextView) getView().findViewById(R.id.descriptionTextView);
        ratingTextView = (TextView) getView().findViewById(R.id.rating_TextView);
        checkpointsLinearLayout = (LinearLayout) getView().findViewById(R.id.checkpointsLinearLayout);

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {

        EventBus.getDefault().unregister(this);

        if (mode) {
            long id = 0;
            ArrayList<Integer> finishedCheckpointsIdList = new ArrayList<>();
            for (CheckBox checkpointsCheckBox : checkpointsCheckBoxes) {
                if (checkpointsCheckBox.isChecked()) {
                    finishedCheckpointsIdList.add(((int) id));
                }
                id++;
            }

            if (!finishedCheckpointsIdList.isEmpty())
                ServiceHelper.finishCheckPoints(getActivity(),
                    AccessTokenGetter.getAccessToken(getActivity().getContentResolver()),
                    taskID, finishedCheckpointsIdList);
        }

        super.onStop();
    }

    @Subscribe(sticky = true)
    public void onOpenTaskEvent(OpenTaskEvent event) {
        taskID = event.taskID;
        userNameTextView.setText(event.userName);
        dateTextView.setText(event.date);
        shortDescriptionTextView.setText(event.shortDescription);
        descriptionTextView.setText(event.description);
        ratingTextView.setText(event.rating);
        String[] selectionArgs = {event.taskID};
        Cursor cursor = getActivity().getContentResolver().query(
                    TaskContract.Checkpoints.CONTENT_URI,
                    TaskContract.Checkpoints.DEFAULT_PROJECTION,
                    null, //"taskID",
                    selectionArgs,
                    null
        );
        mode = checkTaskOwner(event.taskID);
        initializeCheckpoints(cursor, mode);
    }

    private void initializeCheckpoints(Cursor cursor, boolean mode) {
        checkpointsCheckBoxes = new ArrayList<>();
        while (cursor.moveToNext()) {
            CheckBox checkpoint = new CheckBox(getActivity());
            checkpointsCheckBoxes.add(checkpoint);
            checkpoint.setText(cursor.getString(cursor.getColumnIndex(TaskContract.Checkpoints.COLUMN_NAME_TEXT)));
            checkpoint.setClickable(mode);

            if (cursor.getLong(cursor.getColumnIndex(TaskContract.Checkpoints.COLUMN_NAME_STATE)) == 1) {
                checkpoint.setChecked(true);
                checkpoint.setClickable(false);
            }
            checkpointsLinearLayout.addView(checkpoint);
        }
    }

    private boolean checkTaskOwner(String taskID) {
        String[] selectionArgs = {taskID};
        Cursor taskCursor = getActivity().getContentResolver().query(
                TaskContract.Tasks.CONTENT_URI,
                TaskContract.Tasks.DEFAULT_PROJECTION,
                null,
                selectionArgs,
                null
        );
        MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getActivity().getContentResolver());
        if (taskCursor != null && taskCursor.moveToFirst()) {
            if (taskCursor.getString(taskCursor.getColumnIndex(
                    TaskContract.Tasks.COLUMN_NAME_SERVER_ID
            )).equals(myProfile.serverID)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
