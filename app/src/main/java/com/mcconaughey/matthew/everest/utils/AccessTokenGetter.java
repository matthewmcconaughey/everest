package com.mcconaughey.matthew.everest.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;

import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;

/**
 * Created by dmitri on 22.05.16.
 */
public class AccessTokenGetter {

    public static String noAccessToken = "NO_ACCESS_TOKEN";

    public static String getAccessToken(ContentResolver resolver) {

        try (Cursor cursor = resolver.query(ProfileContract.MyProfile.CONTENT_URI,
                ProfileContract.MyProfile.ACCESS_TOKEN_PROJECTION, null, null, null)) {
            assert cursor != null;
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN));

        } catch (CursorIndexOutOfBoundsException e) {
            return noAccessToken;
        }


    }
}
