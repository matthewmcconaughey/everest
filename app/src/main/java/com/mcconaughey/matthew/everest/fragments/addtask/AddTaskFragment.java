package com.mcconaughey.matthew.everest.fragments.addtask;

import android.app.Fragment;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.SwipeDismissItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.touchguard.RecyclerViewTouchActionGuardManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.CreateTaskEvent;
import com.mcconaughey.matthew.everest.fragments.addtask.data.AbstractDataProvider;
import com.mcconaughey.matthew.everest.fragments.addtask.data.CheckPointsDataProvider;
import com.mcconaughey.matthew.everest.fragments.addtask.data.TaskEvent;
import com.mcconaughey.matthew.everest.fragments.addtask.utils.CheckPointsSwipeableAdapter;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.network.services.TaskIntentService;
import com.mcconaughey.matthew.everest.utils.AccessTokenGetter;
import com.mcconaughey.matthew.everest.utils.CheckPoints;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AddTaskFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
    private RecyclerViewSwipeManager mRecyclerViewSwipeManager;
    private RecyclerViewTouchActionGuardManager mRecyclerViewTouchActionGuardManager;
    public FloatingActionButton addTaskFAB;
    private EditText titleEditText;
    private EditText descriptionEditText;

    public AddTaskFragment() {
        super();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_task, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceSaved) {

        super.onActivityCreated(savedInstanceSaved);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Добавление задачи");

        titleEditText = (EditText) getView().findViewById(R.id.titleEditText);
        descriptionEditText = (EditText) getView().findViewById(R.id.descriptionEditText);

        addTaskFAB = (FloatingActionButton) getView().findViewById(R.id.addTaskFAB);
        addTaskFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MainActivity) getActivity()).switchFragments("Feed");
//                sendTask();
                addTask();
            }
        });
    }

    public void sendTask(){
        ArrayList<String> list = new ArrayList<String>();
        String name = ((EditText) getView().findViewById(R.id.titleEditText))
                .getText().toString();
        String description = ((EditText) getView().findViewById(R.id.descriptionEditText))
                .getText().toString();

        String timeStamp = new SimpleDateFormat("dd MM HH:mm").format(Calendar.getInstance().getTime());
        EventBus.getDefault().post(new TaskEvent(name, description, list, timeStamp));
    }

    public void addTask() {
        String shortName = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        ArrayList<CheckPointsDataProvider.CheckPointData> checkPointDatas =
            ((CheckPointsDataProvider) ((CheckPointsSwipeableAdapter) mAdapter)
                    .dataProvider).getCheckPoints();
        CheckPoints checkPoints = new CheckPoints(checkPointDatas);
        MyProfileGetter.Profile profile = MyProfileGetter.getMyProfile(getActivity().getContentResolver());
        ServiceHelper.createTask(getActivity(), profile.accessToken, profile.name + " " + profile.lastName,
                profile.serverID, shortName, description, checkPoints.checkPoints);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //noinspection ConstantConditions
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.checkpointsRecyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());

        // touch guard manager  (this class is required to suppress scrolling while swipe-dismiss animation is running)
        mRecyclerViewTouchActionGuardManager = new RecyclerViewTouchActionGuardManager();
        mRecyclerViewTouchActionGuardManager.setInterceptVerticalScrollingWhileAnimationRunning(true);
        mRecyclerViewTouchActionGuardManager.setEnabled(true);

        // drag & drop manager
        mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
        mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
                (NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.aza));

        // swipe manager
        mRecyclerViewSwipeManager = new RecyclerViewSwipeManager();

        //adapter
        final CheckPointsSwipeableAdapter myItemAdapter = new CheckPointsSwipeableAdapter(getDataProvider());
        myItemAdapter.setEventListener(new CheckPointsSwipeableAdapter.EventListener() {
            @Override
            public void onItemRemoved(int position) {
//                ((MainActivity) getActivity()).onItemRemoved(position);
            }

            @Override
            public void onItemPinned(int position) {

            }

            @Override
            public void onItemViewClicked(View v, boolean pinned) {
                onItemViewClick(v, pinned);
            }
        });

        mAdapter = myItemAdapter;

        mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(myItemAdapter);      // wrap for dragging
        mWrappedAdapter = mRecyclerViewSwipeManager.createWrappedAdapter(mWrappedAdapter);      // wrap for swiping

        final GeneralItemAnimator animator = new SwipeDismissItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Disable the change animation in order to make turning back animation of swiped item works properly.
        animator.setSupportsChangeAnimations(false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);

        // NOTE:
        // The initialization order is very important! This order determines the priority of touch event handling.
        //
        // priority: TouchActionGuard > Swipe > DragAndDrop
        mRecyclerViewTouchActionGuardManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewSwipeManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);


        Button addTaskButton = (Button) getView().findViewById(R.id.addCheckpointButton);
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckPointsSwipeableAdapter) mAdapter).getDataProvider().addItem("");
                mAdapter.notifyItemInserted(((CheckPointsSwipeableAdapter) mAdapter).getDataProvider().getCount() - 1);
            }
        });

    }

    @Override
    public void onPause() {
        mRecyclerViewDragDropManager.cancelDrag();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.release();
            mRecyclerViewDragDropManager = null;
        }

        if (mRecyclerViewSwipeManager != null) {
            mRecyclerViewSwipeManager.release();
            mRecyclerViewSwipeManager = null;
        }

        if (mRecyclerViewTouchActionGuardManager != null) {
            mRecyclerViewTouchActionGuardManager.release();
            mRecyclerViewTouchActionGuardManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;

        super.onDestroyView();
    }

    private void onItemViewClick(View v, boolean pinned) {
        int position = mRecyclerView.getChildAdapterPosition(v);
        if (position != RecyclerView.NO_POSITION) {
            ((MainActivity) getActivity()).onItemClicked(position);
        }
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    public AbstractDataProvider getDataProvider() {
        return ((MainActivity) getActivity()).getDataProvider();
    }

    public void notifyItemChanged(int position) {
        mAdapter.notifyItemChanged(position);
    }

    public void notifyItemInserted(int position) {
        mAdapter.notifyItemInserted(position);
        mRecyclerView.scrollToPosition(position);
    }

    @Subscribe
    public void onCreateTaskEvent(final CreateTaskEvent event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.state) {
                    Toast.makeText(getActivity(), "Task created!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Something is wrong!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
