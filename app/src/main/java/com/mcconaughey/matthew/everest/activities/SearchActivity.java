package com.mcconaughey.matthew.everest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.events.PeopleFoundedEvent;
import com.mcconaughey.matthew.everest.fragments.friends.utils.FriendsAdapter;
import com.mcconaughey.matthew.everest.fragments.friends.utils.Sections;
import com.mcconaughey.matthew.everest.fragments.friends.utils.SimpleSectionedRecyclerViewAdapter;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.AccessTokenGetter;
import com.mcconaughey.matthew.everest.utils.NetworkState;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, FriendsAdapter.AdapterCallback {

    FriendsAdapter adapter;
    RecyclerView recyclerView;


    MenuItem searchItem;
    private SearchView searchView;
    ProgressBar progressBar;
    RelativeLayout searchLayoutContainer;
    LinearLayout emptyMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        progressBar = (ProgressBar) this.findViewById(R.id.progress_bar);

        recyclerView = (RecyclerView) findViewById(R.id.friendsRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        assert recyclerView != null;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new SlideInUpAnimator());

        searchLayoutContainer = (RelativeLayout)findViewById(R.id.search_container);
        emptyMsg = (LinearLayout)findViewById(R.id.empty_msg);

    }

    public String getConnectionType(){
        return NetworkState.getConnectivityStatusString(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_activity_search, menu);


        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle the click on the back arrow click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //handle the back arrow press
        super.onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!Objects.equals(getConnectionType(), "NO_CONNECTION")) {
            ServiceHelper.findPeople(this, AccessTokenGetter.getAccessToken(getContentResolver()), query);
            this.progressBar.setVisibility(View.VISIBLE);
            this.emptyMsg.setVisibility(View.GONE);
        } else {
            Snackbar snackbar = Snackbar
                    .make(searchLayoutContainer, "Проверьте соединение с интернетом", LENGTH_LONG);
            snackbar.show();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Subscribe
    public void onPeopleFoundedEvent(final PeopleFoundedEvent event) throws JSONException {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // тут я получил массив профилей людишек
                ArrayList<FriendsAdapter.FriendItem> list = new ArrayList<>();

                if (event.people == null){

                    progressBar.setVisibility(View.GONE);
                    emptyMsg.setVisibility(View.VISIBLE);

                } else {

                    for (int i = 0; i < event.people.length; i++) {
                        list.add(new FriendsAdapter.FriendItem(
                                event.people[i].lastName + " " + event.people[i].name,
                                event.people[i].serverID));
                    }

                    adapter = new FriendsAdapter(SearchActivity.this);
                    adapter.addValues(list);
                    adapter.notifyDataSetChanged();

                    //This is the code to provide a sectioned list
                    List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

                    Sections secs = new Sections(adapter.getList());
                    int position = 0;
                    for (int i = 0; i < secs.getSize(); i++) {
                        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(position, String.valueOf(secs.getCharacterOfSection(i))));
                        position += secs.getCapacityOfSection(i);
                    }

                    //Add your adapter to the sectionAdapter
                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    final SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                            SimpleSectionedRecyclerViewAdapter(SearchActivity.this, R.layout.section, R.id.section_text, adapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));

                    //Apply this adapter to the RecyclerView
                    recyclerView.setAdapter(mSectionedAdapter);

                    progressBar.setVisibility(View.GONE);

                    mSectionedAdapter.notifyDataSetChanged();
                }


            }
        });
    }

    @Override
    public void onFriendsItemTouchedCallback(String name, String serverID) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("Username", name);
        intent.putExtra("serverID", serverID);
        String accessToken = AccessTokenGetter.getAccessToken(getContentResolver());
        intent.putExtra("accessToken", accessToken);
        startActivity(intent);
    }

}
