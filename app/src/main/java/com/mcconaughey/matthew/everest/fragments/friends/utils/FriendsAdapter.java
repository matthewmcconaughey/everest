package com.mcconaughey.matthew.everest.fragments.friends.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcconaughey.matthew.everest.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dmitri on 02.04.16.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder>  {

    Context context;
    ArrayList<FriendItem> friends;

    private AdapterCallback itemTouchedCallback;

    public FriendsAdapter(Context context) {
        this.context = context;
        this.friends = new ArrayList<>();
        this.itemTouchedCallback = ((AdapterCallback) context);
    }

    public FriendsAdapter(Context context, ArrayList<FriendItem> friendsList) {
        this.context = context;
        this.friends = friendsList;
        this.itemTouchedCallback = ((AdapterCallback) context);
    }

    public void addValues(ArrayList<FriendItem> friends) {
        this.friends.addAll(friends);
        // add elements to al, including duplicates
        Set<FriendItem> hs = new HashSet<>();
        hs.addAll(this.friends);
        this.friends.clear();
        this.friends.addAll(hs);
    }

    public void setFriends(ArrayList<FriendItem> friends) {
        this.friends = friends;
    }

    public ArrayList<FriendItem> getList() {
        if (friends != null) {
            return friends;
        } else {
            friends = new ArrayList<>();
            return friends;
        }
    }

    public void clearList() {
        friends.clear();
        friends.addAll(new ArrayList<FriendItem>());
        this.notifyDataSetChanged();
    }

    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_item, parent, false);
        return new FriendsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FriendsViewHolder holder, int position) {
        holder.userName.setText(this.friends.get(position).name);
        holder.avatar.setImageDrawable(context.getResources().getDrawable(R.drawable.foo));
        holder.serverID = this.friends.get(position).serverID;
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {

        View container;
        TextView userName;
        ImageView avatar;
        String serverID;

        public FriendsViewHolder(View itemView) {
            super(itemView);

            this.container = (View) itemView.findViewById(R.id.friend);
            this.userName = (TextView) itemView.findViewById(R.id.user_name_friends);
            this.avatar = (ImageView) itemView.findViewById(R.id.user_avatar_friends);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemTouchedCallback.onFriendsItemTouchedCallback(
                            (String) ((TextView) container.findViewById(R.id.user_name_friends)).getText(),
                            serverID);
                }
            });
        }
    }

    public interface AdapterCallback {
        void onFriendsItemTouchedCallback(String name, String serverID);
    }

    public static class FriendItem {
        public String name;
        public String serverID;

        public FriendItem(String name, String serverID) {
            this.name = name;
            this.serverID = serverID;
        }
    }
}
