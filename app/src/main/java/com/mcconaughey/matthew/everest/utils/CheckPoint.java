package com.mcconaughey.matthew.everest.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dmitri on 06.04.16.
 */
public class CheckPoint implements Parcelable {
    public String taskID;
    public long id;
    public String text;
    public int state;

    public CheckPoint() {
        this.taskID = "";
        this.id = 0;
        this.text = "";
        this.state = 0;
    }

    public CheckPoint(String taskID, long id, String text, int state) {
        this.taskID = taskID;
        this.id = id;
        this.text = text;
        this.state = state;
    }

    protected CheckPoint(Parcel in) {
        taskID = in.readString();
        id = in.readLong();
        text = in.readString();
        state = in.readInt();
    }

    public static final Creator<CheckPoint> CREATOR = new Creator<CheckPoint>() {
        @Override
        public CheckPoint createFromParcel(Parcel in) {
            return new CheckPoint(in);
        }

        @Override
        public CheckPoint[] newArray(int size) {
            return new CheckPoint[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(taskID);
        dest.writeLong(id);
        dest.writeString(text);
        dest.writeInt(state);
    }
}
