package com.mcconaughey.matthew.everest.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.SignInEvent;
import com.mcconaughey.matthew.everest.fragments.sign.SignInFragment;
import com.mcconaughey.matthew.everest.fragments.sign.SignUpFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SignActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    Fragment signInFragment;
    Fragment signUpFragment;


    RelativeLayout mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        this.fragmentManager = getFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        signInFragment = new SignInFragment();
        transaction.replace(R.id.fragmentContainer, signInFragment);
        transaction.commit();

        getSupportActionBar().hide();

        mainContainer = (RelativeLayout)findViewById(R.id.main_container);
        getSupportLoaderManager().initLoader(0, null, this);
    }

    public RelativeLayout getMainContainer() {
        return mainContainer;
    }

    public void switchFragments(String fragmentName) {
        transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        switch (fragmentName) {
            case "SignIn":
                transaction.replace(R.id.fragmentContainer, signInFragment);
                break;
            case "SignUp":
                if (signUpFragment == null)
                    signUpFragment = new SignUpFragment();
                transaction.replace(R.id.fragmentContainer, signUpFragment);
                break;
            default:
                break;
        }
        transaction.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onSignInEvent(SignInEvent event) {
        // TODO: сохранить токен
        if (!event.accessToken.equals("error")) {
            startActivity(new Intent(SignActivity.this, MainActivity.class));
            this.finish();
        } else {
            Snackbar snack = Snackbar.make(mainContainer, "Что-то пошло не так, попробуйте заново", Snackbar.LENGTH_LONG);
            View view = snack.getView();
            FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.TOP;
            view.setLayoutParams(params);
            snack.show();
        }

        // НЕ ТРОГАТЬ, ДЛЯ ОТЛАДКИ МОГЕТ ПРИГОДИТЬСЯ
//        ContentValues values = new ContentValues();
//        values.put(ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN, event.accessToken);
//        values.put(ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID, "5714cfa6a868b01de6ccc201");
//        values.put(ProfileContract.MyProfile.COLUMN_NAME_EMAIL, 1);
//        values.put(ProfileContract.MyProfile.COLUMN_NAME_NAME, "Alexander");
//        values.put(ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME, "Borodin");
//        getActivity().getContentResolver().insert(ProfileContract.MyProfile.CONTENT_URI, values);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTACT_URI = ProfileContract.MyProfile.CONTENT_URI;
        return new CursorLoader(this, CONTACT_URI, null,
                null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            cursor.moveToLast();

//            String email = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_EMAIL));

            // TODO : вот здеся можно эвентбасом посылать аксесс токен куда нужно
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {}

}
