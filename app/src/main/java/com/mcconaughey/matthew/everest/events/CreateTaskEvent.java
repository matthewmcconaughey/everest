package com.mcconaughey.matthew.everest.events;

/**
 * Created by dmitri on 28.05.16.
 */
public class CreateTaskEvent {
    public boolean state;

    public CreateTaskEvent(boolean state) {
        this.state = state;
    }
}
