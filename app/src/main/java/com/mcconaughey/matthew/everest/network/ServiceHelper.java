package com.mcconaughey.matthew.everest.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.events.SignInEvent;
import com.mcconaughey.matthew.everest.events.SignUpEvent;
import com.mcconaughey.matthew.everest.fragments.addtask.data.CheckPointsDataProvider;
import com.mcconaughey.matthew.everest.network.services.ProfileIntentService;
import com.mcconaughey.matthew.everest.network.services.SignIntentService;
import com.mcconaughey.matthew.everest.network.services.TaskIntentService;
import com.mcconaughey.matthew.everest.utils.CheckPoint;
import com.mcconaughey.matthew.everest.utils.CheckPoints;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


public class ServiceHelper {

    public static void signIn(final Context context, final String email, final String password) {

        final IntentFilter filter = new IntentFilter();
        filter.addAction(SignIntentService.ACTION_SIGN_IN);

        LocalBroadcastManager.getInstance(context).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                final String accessToken = intent.getStringExtra(SignIntentService.EXTRA_SIGN_IN_RESULT);
                EventBus.getDefault().post(new SignInEvent(accessToken));
            }
        }, filter);

        Intent intent = new Intent(context, SignIntentService.class);
        intent.setAction(SignIntentService.ACTION_SIGN_IN);
        intent.putExtra(SignIntentService.EXTRA_EMAIL, email);
        intent.putExtra(SignIntentService.EXTRA_PASSWORD, password);
        context.startService(intent);

    }

    public static void signUp(final Context context, final String email, final String password,
                              final String name, final String surname) {

        final IntentFilter filter = new IntentFilter();
        filter.addAction(SignIntentService.ACTION_SIGN_UP);

        LocalBroadcastManager.getInstance(context).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                final String accessToken = intent.getStringExtra(SignIntentService.EXTRA_SIGN_UP_ACCESS_TOKEN);
                final String serverId = intent.getStringExtra(SignIntentService.EXTRA_SIGN_UP_SERVER_ID);
                final String email = intent.getStringExtra(SignIntentService.EXTRA_SIGN_UP_EMAIL);
                final String name = intent.getStringExtra(SignIntentService.EXTRA_SIGN_UP_NAME);
                final String lastName = intent.getStringExtra(SignIntentService.EXTRA_SIGN_UP_LAST_NAME);

                EventBus.getDefault().post(new SignUpEvent(accessToken, serverId, email, name, lastName));
            }
        }, filter);

        Intent intent = new Intent(context, SignIntentService.class);
        intent.setAction(SignIntentService.ACTION_SIGN_UP);
        intent.putExtra(SignIntentService.EXTRA_EMAIL, email);
        intent.putExtra(SignIntentService.EXTRA_PASSWORD, password);
        intent.putExtra(SignIntentService.EXTRA_NAME, name);
        intent.putExtra(SignIntentService.EXTRA_SURNAME, surname);
        context.startService(intent);

    }

    public static void getSubscribes(final Context context, final String accessToken, final String serverID, int mode) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_GET_SUBSCRIBES);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_SERVER_ID, serverID);
        intent.putExtra(ProfileIntentService.EXTRA_MODE, mode);
        context.startService(intent);
    }

    public static void getProfile(final Context context, final String accessToken, final String id) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_GET_PROFILE);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_SERVER_ID, id);
        context.startService(intent);
    }

    public static void updateProfile(final Context context, final String accessToken,
                                     int field, String value) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_UPDATE_PROFILE);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_FIELD_TO_UPDATE, String.valueOf(field));
        intent.putExtra(ProfileIntentService.EXTRA_VALUE, value);
        context.startService(intent);
    }

    public static void findPeople(final Context context, final String accessToken,
                                     String query) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_FIND_PEOPLE);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_QUERY, query);
        context.startService(intent);
    }


    public static void subscribe(final Context context, final String accessToken,
                                 String myServerID, String notMyServerID) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_SUBSCRIBE);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_SERVER_ID, myServerID);
        intent.putExtra(ProfileIntentService.EXTRA_NOT_MY_SERVER_ID, notMyServerID);
        context.startService(intent);

    }

    public static void rate(final Context context, final String serverID, final String accessToken,
                            final String taskID, final long itemId) {

        Intent intent = new Intent(context, TaskIntentService.class);
        intent.setAction(TaskIntentService.ACTION_RATE_TASK);
        intent.putExtra(TaskIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(TaskIntentService.EXTRA_SERVER_ID, taskID);
        intent.putExtra(TaskIntentService.EXTRA_CREATOR_ID, serverID);
        intent.putExtra(TaskIntentService.EXTRA_ITEM_ID, String.valueOf(itemId));
        context.startService(intent);

        // serverId - айдишник таска на сервере
        // creator id - мой айдишник
    }

    public static void unsubscribe(final Context context, final String accessToken,
                                   String myServerID, String notMyServerID) {
        Intent intent = new Intent(context, ProfileIntentService.class);
        intent.setAction(ProfileIntentService.ACTION_UNSUBSCRIBE);
        intent.putExtra(ProfileIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(ProfileIntentService.EXTRA_SERVER_ID, myServerID);
        intent.putExtra(ProfileIntentService.EXTRA_NOT_MY_SERVER_ID, notMyServerID);
        context.startService(intent);
    }

    public static void createTask(final Context context, String accessToken, String userName, String serverID,
                                  String shortName, String description, ArrayList<CheckPoint> checkPoints) {
        Intent intent = new Intent(context, TaskIntentService.class);
        intent.setAction(TaskIntentService.ACTION_CREATE_TASK);
        intent.putExtra(TaskIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(TaskIntentService.EXTRA_AUTHOR_NAME, userName);
        intent.putExtra(TaskIntentService.EXTRA_SERVER_ID, serverID);
        intent.putExtra(TaskIntentService.EXTRA_SHORT_NAME, shortName);
        intent.putExtra(TaskIntentService.EXTRA_DESCRIPTION, description);
//        intent.putExtra(TaskIntentService.EXTRA_CHECKPOINTS, checkPoints);
        intent.putParcelableArrayListExtra(TaskIntentService.EXTRA_CHECKPOINTS, checkPoints);
        context.startService(intent);
    }

    public static void getFeed(final Context context, String accessToken, ArrayList<String> subscribes) {
        Intent intent = new Intent(context, TaskIntentService.class);
        intent.setAction(TaskIntentService.ACTION_GET_FEED);
        intent.putExtra(TaskIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putStringArrayListExtra(TaskIntentService.EXTRA_SUBSCRIBES, subscribes);
        context.startService(intent);
    }


    public static void finishCheckPoints(final Context context, String accessToken,
                                         String taskID, ArrayList<Integer> finishedCheckpoints) {
        Intent intent = new Intent(context, TaskIntentService.class);
        intent.setAction(TaskIntentService.ACTION_FINISH_CHECKPOINTS);
        intent.putExtra(TaskIntentService.EXTRA_ACCESS_TOKEN, accessToken);
        intent.putExtra(TaskIntentService.EXTRA_TASK_ID, taskID);
        intent.putIntegerArrayListExtra(TaskIntentService.EXTRA_FINISHED_CHECKPOINTS, finishedCheckpoints);
        context.startService(intent);
    }
}
