package com.mcconaughey.matthew.everest.network;

import com.mcconaughey.matthew.everest.utils.CheckPoint;
import com.mcconaughey.matthew.everest.utils.DeviceName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by alexander on 16.04.16.
 */

public interface REST {

    public static final String BASE_URL = "http://192.168.43.232:8080";

    class SignInBody {
        public String email;
        public String password;
        public String device;


        public SignInBody(String email, String password) {
            this.email = email;
            this.password = password;
            this.device = DeviceName.getDeviceName();
        }
    }

    class SignUpBody extends SignInBody {
        public String name;
        public String last_name;

        public SignUpBody(String email, String password, String name, String last_name) {
            super(email, password);
            this.name = name;
            this.last_name = last_name;
        }

    }

    class SignUpResult {
        public String accessToken;
        public String serverID;
        public String email;
        public String name;
        public String lastName;
    }

    public class Profile {
        public String serverID;
        public String email;
        public String name;
        public String lastName;
        public String subscribesCount;
        public String subscribersCount;
        public String tasksCount;
        public String completedTasksCount;
        public String about;
    }

    class ProfileRequestBody {
        public String accessToken;
        public String serverID;

        public ProfileRequestBody(String accessToken, String serverID) {
            this.accessToken = accessToken;
            this.serverID = serverID;
        }
    }

    class GetSubscribesResult {
        public Profile[] subscribes;
    }

    class UpdateAboutBody {
        public String accessToken;
        public String about;

        public UpdateAboutBody(String accessToken, String about) {
            this.accessToken = accessToken;
            this.about = about;
        }
    }

    class UpdateEmailBody {
        public String accessToken;
        public String email;

        public UpdateEmailBody(String accessToken, String email) {
            this.accessToken = accessToken;
            this.email = email;
        }
    }

    class UpdateResult {
        boolean result;
    }

    class FindPeopleBody {
        public String accessToken;
        public String query;

        public FindPeopleBody(String accessToken, String query) {
            this.accessToken = accessToken;
            this.query = query;
        }
    }

    class FindPeopleResult {
        public Profile[] people;
    }

    class SubscribeBody {
        public String accessToken;
        public String my_id;
        public String not_my_id;

        public SubscribeBody(String accessToken, String myID, String notMyID) {
            this.accessToken = accessToken;
            this.my_id = myID;
            this.not_my_id = notMyID;
        }
    }

    class UnubscribeResult {
        boolean result;
    }

    class CreateTaskBody {
        public String accessToken;
        public String authorName;
        public String serverID;
        public String shortName;
        public String description;
        public String creationTime;
        public ArrayList<CheckPoint> undoneCheckpoints;

        public CreateTaskBody(String accessToken, String authorName, String serverID, String shortName,
                String description, String creationTime, ArrayList<CheckPoint> undoneCheckpoints) {
            this.accessToken = accessToken;
            this.authorName = authorName;
            this.serverID = serverID;
            this.shortName = shortName;
            this.description = description;
            this.creationTime = creationTime;
            this.undoneCheckpoints = undoneCheckpoints;
        }
    }

    class RateTaskBody {
        public String serverID;
        public String creatorID;
        public String accessToken;

        public RateTaskBody(String serverID, String creatorID, String accessToken) {
            this.creatorID = creatorID;
            this.serverID = serverID;
            this.accessToken = accessToken;
        }
    }

    class Task {
        public String taskID;
        public String authorName;
        public String serverID;
        public String shortName;
        public String description;
        public String creationTime;
        public ArrayList<CheckPoint> undoneCheckpoints;
        public Long rating;
        public String rated;
    }

    class TaskRateResult {
        public String resp;
        public String rating;
    }

    class GetFeedBody {
        public String accessToken;
        public ArrayList<String> subscribes;

        public GetFeedBody(String accessToken, ArrayList<String> subscribes) {
            this.accessToken = accessToken;
            this.subscribes = subscribes;
        }
    }

    class GetFeedResult {
        ArrayList<Task> feed;
    }

    class FinishCheckpointsBody {
        String accessToken;
        String taskID;
        ArrayList<Long> finishedCheckpoints;

        public FinishCheckpointsBody(String accessToken, String taskID,
                                     ArrayList<Integer> finishedCheckpoints) {
            this.accessToken = accessToken;
            this.taskID = taskID;
            this.finishedCheckpoints = new ArrayList<>();
            for (Integer checkPointID : finishedCheckpoints) {
                this.finishedCheckpoints.add(checkPointID.longValue());
            }
        }
    }

    class FinishCheckpointsResult {
        boolean result;
    }

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/signin")
    Call<SignUpResult> signIn(@Body SignInBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/create")
    Call<SignUpResult> signUp(@Body SignUpBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/subscribes")
    Call<GetSubscribesResult> getSubscribes(@Body ProfileRequestBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/subscribers")
    Call<GetSubscribesResult> getSubscribers(@Body ProfileRequestBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/get")
    Call<Profile> getProfile(@Body ProfileRequestBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/update")
    Call<UpdateResult> updateAbout(@Body UpdateAboutBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/update")
    Call<UpdateResult> updateEmail(@Body UpdateEmailBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/find")
    Call<FindPeopleResult> findPeople(@Body FindPeopleBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/subscribe")
    Call<Profile> subscribe(@Body SubscribeBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/unsubscribe")
    Call<UnubscribeResult> unsubscribe(@Body SubscribeBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/create_task")
    Call<Task> createTask(@Body CreateTaskBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/rate_task")
    Call<TaskRateResult> rateTask(@Body RateTaskBody body);


    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/get_feed")
    Call<GetFeedResult> getFeed(@Body GetFeedBody body);

    @Headers({
            "Content-type : application/json",
            "charset : UTF-8"
    })
    @POST("/finish_checkpoints")
    Call<FinishCheckpointsResult> finishCheckpoints(@Body FinishCheckpointsBody body);
}
