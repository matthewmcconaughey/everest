package com.mcconaughey.matthew.everest.fragments.addtask.data;

import java.util.ArrayList;

/**
 * Created by alexander on 25.05.16.
 */
public class TaskEvent {
    public final String name;
    public final String description;
    public final String time;
    public final ArrayList<String> checkpoints;

    public TaskEvent(String name, String description, ArrayList<String> checkpoints, String time) {
        this.name = name;
        this.description = description;
        this.checkpoints = checkpoints;
        this.time = time;
    }
}