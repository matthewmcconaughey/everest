package com.mcconaughey.matthew.everest.fragments.friends.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by dmitri on 03.04.16.
 */
public class Sections {
    private ArrayList<Integer> sectionsCapacityArray;
    private ArrayList<Character> sectionCharacter;
    private int size = 0;

    public Sections(ArrayList<FriendsAdapter.FriendItem> people) {
        sectionsCapacityArray = new ArrayList<>();
        sectionCharacter = new ArrayList<>();

        Collections.sort(people, new FriendItemComparator());

        char currentLetter = Character.toUpperCase(people.get(0).name.charAt(0));
        sectionCharacter.add(currentLetter);
        sectionsCapacityArray.add(0);
        size++;

        int currentCapacity = 0;

        for (FriendsAdapter.FriendItem person : people) {
            if (Character.toUpperCase(person.name.charAt(0)) == currentLetter) {
                sectionsCapacityArray.set(size - 1, ++currentCapacity);
            } else {
                sectionsCapacityArray.add(1);
                currentCapacity = 1;
                currentLetter = Character.toUpperCase(person.name.charAt(0));
                sectionCharacter.add(currentLetter);
                size++;
            }
        }
    }

    public int getSize() {
        return this.size;
    }

    public int getCapacityOfSection(int index) {
        return this.sectionsCapacityArray.get(index);
    }

    public char getCharacterOfSection(int index) {
        return this.sectionCharacter.get(index);
    }

    public class FriendItemComparator implements Comparator<FriendsAdapter.FriendItem> {
        @Override
        public int compare(FriendsAdapter.FriendItem lhs, FriendsAdapter.FriendItem rhs) {
            return lhs.name.compareTo(rhs.name);
        }
    }
}
