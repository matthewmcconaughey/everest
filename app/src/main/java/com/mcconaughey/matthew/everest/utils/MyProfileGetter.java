package com.mcconaughey.matthew.everest.utils;

import android.content.ContentResolver;
import android.database.Cursor;

import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.network.REST;

/**
 * Created by denis on 23.05.16.
 */
public class MyProfileGetter {

    public static class Profile extends REST.Profile {
        public String accessToken;
    }

    public static Profile getMyProfile(ContentResolver resolver) {
        Cursor cursor = resolver.query(ProfileContract.MyProfile.CONTENT_URI,
                ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
        cursor.moveToFirst();
        Profile result = new Profile();
        result.serverID = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID));
        result.email = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_EMAIL));
        result.name = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_NAME));
        result.lastName = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME));
//        result.subscribesCount = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_SUBSCRIBES_COUNT));
//        result.subscribersCount = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_SUBSCRIBERS_COUNT));
//        result.tasksCount = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_TASKS_COUNT));
//        result.completedTasksCount = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_COMPLETED_TASKS_COUNT));
//        result.about = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_ABOUT));
        result.accessToken = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN));
        cursor.close();
        return result;
    }

    public static String getMyServerID(ContentResolver resolver) {
        Cursor cursor = resolver.query(ProfileContract.MyProfile.CONTENT_URI,
                ProfileContract.MyProfile.SERVER_ID_PROJECTION, null, null, null);
        cursor.moveToFirst();
        String result = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID));
        cursor.close();
        return result;
    }
}
