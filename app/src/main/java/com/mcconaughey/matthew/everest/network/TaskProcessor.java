package com.mcconaughey.matthew.everest.network;

import android.content.ContentResolver;

import com.mcconaughey.matthew.everest.events.RateTaskEvent;
import com.mcconaughey.matthew.everest.utils.CheckPoint;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dmitri on 28.05.16.
 */
public class TaskProcessor {
    public static boolean processCreateTask(ContentResolver contentResolver, String accessToken, String authorName, String serverID,
                                         String shortName, String description, ArrayList<CheckPoint> checkPoints) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
        Date now = new Date();
        String strDate = sdf.format(now);


        Call<REST.Task> call = rest.createTask(new REST.CreateTaskBody(accessToken, authorName, serverID,
                shortName, description, strDate, checkPoints));

        Response<REST.Task> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result != null && result.body() != null;
    }

    public static RateTaskEvent processLikeTask(ContentResolver contentResolver, String creatorId,
                                                String serverId, String accessToken){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.TaskRateResult> call = rest.rateTask(new REST.RateTaskBody(creatorId, serverId, accessToken));

        Response<REST.TaskRateResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result != null){
            return new RateTaskEvent(true, result.body().resp, result.body().rating, serverId);
        }

        return new RateTaskEvent(true, "1", "123", "0");

    }

    public static ArrayList<REST.Task> processGetFeed(String accessToken, ArrayList<String> subscribes) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.GetFeedResult> call = rest.getFeed(new REST.GetFeedBody(accessToken, subscribes));

        Response<REST.GetFeedResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result != null && result.body() != null ? result.body().feed : null;
    }

    public static boolean processFinishTask(String accessToken, String taskID,
                                            ArrayList<Integer> finishedCheckPoints) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.FinishCheckpointsResult> call = rest.finishCheckpoints(new REST.FinishCheckpointsBody(accessToken,
                taskID, finishedCheckPoints));

        Response<REST.FinishCheckpointsResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result != null && result.body() != null;
    }
}

