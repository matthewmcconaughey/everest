package com.mcconaughey.matthew.everest.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.utils.AccessTokenGetter;
import com.stephentuso.welcome.WelcomeScreenHelper;
import com.stephentuso.welcome.ui.WelcomeActivity;

public class SplashActivity extends Activity {

    protected int splashScreenTime = 1;
    WelcomeScreenHelper welcomeScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences preferences = getSharedPreferences("my_preferences", MODE_PRIVATE);

        if (!preferences.getBoolean("on_boarding_complete",false)) {
            welcomeScreen = new WelcomeScreenHelper(this, OnBoardingScreen.class);
            welcomeScreen.show(savedInstanceState);
            preferences.edit()
                    .putBoolean("on_boarding_complete",true).apply();
        } else {
            showSplash();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == WelcomeScreenHelper.DEFAULT_WELCOME_SCREEN_REQUEST) {
            showSplash();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        welcomeScreen.onSaveInstanceState(outState);
    }

    private void startMainActivity(){
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }

    private void startSignActivity(){
        startActivity(new Intent(SplashActivity.this, SignActivity.class));
        finish();
    }

    public void showSplash(){
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(splashScreenTime * 1000);
                } catch (Exception ignored) {

                } finally {
                    if (AccessTokenGetter.getAccessToken(getContentResolver()).equals(AccessTokenGetter.noAccessToken)) {
                        startSignActivity();
                    } else {
                        startMainActivity();
                    }
                }
            }
        };

        splashTread.start();
    }

}

