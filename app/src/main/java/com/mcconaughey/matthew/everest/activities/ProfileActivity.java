package com.mcconaughey.matthew.everest.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.GetProfileEvent;
import com.mcconaughey.matthew.everest.events.SubscribeStateEvent;
import com.mcconaughey.matthew.everest.network.REST;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    CollapsingToolbarLayout collapsingToolbarLayout;
    String accessToken;
    String serverID;

    String profile;

    FloatingActionButton fab;

    boolean state; // true if i am already subscribed on this dude, false if not

    public static final String MY_PROFILE_STATE = "my";

    private static final int PROFILE_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.initToolbar();



        this.setPopUpBlocksOnClickListeners();

        View friendsView = findViewById(R.id.friendsView);
        View tasksView = findViewById(R.id.tasksView);

        if (friendsView != null && tasksView != null) {
            friendsView.setOnClickListener(this);
            tasksView.setOnClickListener(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent.getStringExtra("Username") != null) {
            collapsingToolbarLayout.setTitle(getIntent().getStringExtra("Username"));
            serverID = getIntent().getStringExtra("serverID");
            accessToken = getIntent().getStringExtra("accessToken");
            profile = getIntent().getStringExtra("profile");
            getSupportLoaderManager().initLoader(PROFILE_LOADER, null, this);

            String[] select = {serverID};
            EventBus.getDefault().register(this);
            Cursor cursor = getContentResolver().query(ProfileContract.Profiles.CONTENT_URI,
                            ProfileContract.Profiles.FULL_PROFILE_PROJECTION, null, select, null);
            if (cursor != null) {
                EventBus.getDefault().post(new GetProfileEvent(cursor));
            }
            if (serverID != null && accessToken != null) {
                ServiceHelper.getProfile(this, accessToken, serverID);
            }

            this.fillFab();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("-");
    }

    private void setPopUpBlocksOnClickListeners() {
        setListener("Редактирование информации о себе:", R.id.aboutLinearLayout, R.id.aboutTextView);
    }

    private void setListener(final String title, int clickableLayout, final int edit) {
        LinearLayout layout = (LinearLayout) findViewById(clickableLayout);
        assert layout != null;
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profile != null) {
                    if (profile.equals(MY_PROFILE_STATE)) {
                        showPopUp(title,
                                ((TextView) findViewById(edit)).getText().toString(), edit);
                    }
                }
            }
        });
    }

    private void showPopUp(String title, final String editValue, final int anchorId) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .customView(R.layout.popup_window, true)
                .positiveText("Save")
                .negativeText(android.R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText editText = (EditText) dialog.getView().findViewById(R.id.edit_popup);
                        ServiceHelper.updateProfile(ProfileActivity.this, accessToken, anchorId,
                                editText.getText().toString());
                        ((TextView) findViewById(anchorId)).setText(editText.getText());
                    }
                }).build();

        TextView fieldLabel = (TextView) dialog.findViewById(R.id.edit_popup);
        fieldLabel.setText(editValue);
        dialog.show();
    }

    private void fillFab() {

        String[] select = {serverID};
        Cursor cursor = getContentResolver().query(ProfileContract.MySubscribes.CONTENT_URI,
                ProfileContract.MySubscribes.DEFAULT_PROJECTION, null, select, null);
        fab = (FloatingActionButton) findViewById(R.id.floating_action_button);
        cursor.moveToFirst();

        if (cursor.getCount() == 0) {
            fab.setImageDrawable(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_plus).actionBar().color(Color.WHITE));
            state = false;
        } else {
            fab.setImageDrawable(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_check).actionBar().color(Color.WHITE));
            state = true;
        }
        fab.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        //handle the back arrow press
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle the click on the back arrow click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.friendsView:
                break;
            case R.id.tasksView:
                Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                intent.putExtra("fragment", "UserTasks");
                startActivity(intent);
                break;
            case R.id.floating_action_button:
                if (!state) {
                    MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getContentResolver());
                    ServiceHelper.subscribe(this, myProfile.accessToken, myProfile.serverID, serverID);
                    state = true;
                } else {
                    MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getContentResolver());
                    ServiceHelper.unsubscribe(this, myProfile.accessToken, myProfile.serverID, serverID);
                    state = false;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTACT_URI = null;
        String[] selection = null;
        switch (id) {
            case PROFILE_LOADER:
                CONTACT_URI = ProfileContract.Profiles.CONTENT_URI;
                selection = new String[]{serverID};
                break;
        }
        return new CursorLoader(this, CONTACT_URI, null,
                null, selection, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null) {
            switch (loader.getId()) {
                case PROFILE_LOADER:
                    if(cursor.moveToFirst()) {
                        EventBus.getDefault().post(new GetProfileEvent(cursor));
                    }
                    break;
            }
        }
    }

    @Subscribe
    public void onGetProfileEvent(GetProfileEvent event) {
        setValueToTextView(event.profile, R.id.friendsCountTextView, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBES_COUNT);
        setValueToTextView(event.profile, R.id.subscribersCountTextView, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBERS_COUNT);
        setValueToTextView(event.profile, R.id.tasksCountTextView, ProfileContract.Profiles.COLUMN_NAME_TASKS_COUNT);
        setValueToTextView(event.profile, R.id.completedTasksCountTextView, ProfileContract.Profiles.COLUMN_NAME_COMPLETED_TASKS_COUNT);
        setValueToTextView(event.profile, R.id.emailTextView, ProfileContract.Profiles.COLUMN_NAME_EMAIL);
        setValueToTextView(event.profile, R.id.aboutTextView, ProfileContract.Profiles.COLUMN_NAME_ABOUT);
    }

    @Subscribe
    public void onSubscribeStateEvent(final SubscribeStateEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (event.state) {
                    fab.setImageDrawable(new IconicsDrawable(ProfileActivity.this, GoogleMaterial.Icon.gmd_check)
                            .actionBar().color(Color.WHITE));
                } else {
                    fab.setImageDrawable(new IconicsDrawable(ProfileActivity.this, GoogleMaterial.Icon.gmd_plus)
                            .actionBar().color(Color.WHITE));
                }
            }
        });

    }

    private void setValueToTextView(Cursor cursor, int textViewId, String cursorColumnName) {
        TextView textView = (TextView) findViewById(textViewId);
        if (textView != null) {
            String value = cursor.getString(cursor.getColumnIndex(cursorColumnName));
            if (!value.isEmpty() && value != null) {
                textView.setText(value);
            } else {
                textView.setText("-");
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
