package com.mcconaughey.matthew.everest.utils;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.fragments.addtask.data.AbstractDataProvider;
import com.mcconaughey.matthew.everest.fragments.addtask.utils.ViewUtils;

import java.util.ArrayList;

/**
 * Created by dmitri on 06.04.16.
 */
public class CheckPointsAdapter extends RecyclerView.Adapter<CheckPointsAdapter.CheckPointViewHolder>
        implements DraggableItemAdapter<CheckPointsAdapter.CheckPointViewHolder> {

    private Context context;
    private ArrayList<CheckPoint> checkpoints;

    public CheckPointsAdapter(Context context) {
        this.context = context;
    }

    public CheckPointsAdapter(Context context, AbstractDataProvider dataProvider, ArrayList<CheckPoint> checkPoints) {
        this.context = context;
//        this.mProvider = dataProvider;
        this.checkpoints = checkPoints;
        setHasStableIds(true);
    }

    @Override
    public CheckPointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkpoint_item, parent, false);
        return new CheckPointViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CheckPointViewHolder holder, int position) {
        holder.state.setActivated((this.checkpoints.get(position).state == 1));
        holder.description.setText(this.checkpoints.get(position).text);
    }

    @Override
    public int getItemCount() {
        return checkpoints.size();
    }

    @Override
    public long getItemId(int position) {
        return 10;
    }

    @Override
    public boolean onCheckCanStartDrag(CheckPointViewHolder holder, int position, int x, int y) {
        // x, y --- relative from the itemView's top-left
        final View containerView = holder.container;
        final View dragHandleView = holder.dragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return ViewUtils.hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(CheckPointViewHolder holder, int position) {
        return null;
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

//        mProvider.moveItem(fromPosition, toPosition);

        notifyItemMoved(fromPosition, toPosition);
    }

    public class CheckPointViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout container;
        CheckBox state;
        EditText description;
        public View dragHandle;


        public CheckPointViewHolder(View itemView) {
            super(itemView);

            this.container = (RelativeLayout) itemView.findViewById(R.id.container);
            this.state = (CheckBox) itemView.findViewById(R.id.checkbox);
            this.description = (EditText) itemView.findViewById(R.id.checkpoint_description_edt);
            this.dragHandle = itemView.findViewById(R.id.drag_handle);
        }
    }
}
