package com.mcconaughey.matthew.everest.events;

/**
 * Created by alexander on 16.04.16.
 */
public class SignUpEvent {
    public String accessToken;
    public String serverID;
    public String email;
    public String name;
    public String lastName;


    public SignUpEvent(String accessToken, String serverID, String email,
                       String name, String lastName) {
        this.accessToken = accessToken;
        this.serverID = serverID;
        this.email = email;
        this.name = name;
        this.lastName = lastName;
    }

    public int getSize() {
        return 5;
    }

}
