package com.mcconaughey.matthew.everest.database.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.mcconaughey.matthew.everest.database.DataBaseHelper;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;

import java.util.HashMap;

/**
 * Created by dmitri on 28.05.16.
 */
public class TaskContentProvider extends ContentProvider {

    private DataBaseHelper dbHelper;

    private static HashMap<String, String> tasksDefaultProjection;
    private static HashMap<String, String> checkpointsDefaultProjection;

    private static final Integer TASKS = 1;
    private static final Integer CHECKPOINTS = 2;

    private static final HashMap<String, Integer> sUriMatcher;

    static {
        sUriMatcher = new HashMap<>();
        sUriMatcher.put(TaskContract.SCHEME + TaskContract.AUTHORITY + "/tasks", TASKS);
        sUriMatcher.put(TaskContract.SCHEME + TaskContract.AUTHORITY + "/checkpoints", CHECKPOINTS);

        tasksDefaultProjection = new HashMap<>();
        for(int i=0; i < TaskContract.Tasks.DEFAULT_PROJECTION.length; i++) {
            tasksDefaultProjection.put(
                    TaskContract.Tasks.DEFAULT_PROJECTION[i],
                    TaskContract.Tasks.DEFAULT_PROJECTION[i]);
        }

        checkpointsDefaultProjection = new HashMap<>();
        for(int i = 0; i < TaskContract.Checkpoints.DEFAULT_PROJECTION.length; i++) {
            checkpointsDefaultProjection.put(
                    TaskContract.Checkpoints.DEFAULT_PROJECTION[i],
                    TaskContract.Checkpoints.DEFAULT_PROJECTION[i]);
        }

    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        if (!uriChecker(uri)) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        dbHelper = new DataBaseHelper(getContext());

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String orderBy = null;
        switch (sUriMatcher.get(u)) {
            case 1: // TASKS
                qb.setTables(TaskContract.Tasks.TABLE_NAME);
                qb.setProjectionMap(tasksDefaultProjection);
                orderBy = TaskContract.Tasks.COLUMN_NAME_TASK_ID + " DESC";
                if (selectionArgs != null) {
                    qb.appendWhere(TaskContract.Tasks.COLUMN_NAME_TASK_ID + "= ?");
                }
                break;
            case 2: // CHECKPOINTS
                qb.setTables(TaskContract.Checkpoints.TABLE_NAME);
                qb.setProjectionMap(checkpointsDefaultProjection);
                orderBy = TaskContract.Checkpoints.COLUMN_NAME_ID + " ASC";
                if (selectionArgs != null)
                    qb.appendWhere(TaskContract.Checkpoints.COLUMN_NAME_TASK_ID + "= ?");
                break;
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (!uriChecker(uri)) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();

        dbHelper = new DataBaseHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long rowId = -1;
        Uri rowUri = Uri.EMPTY;
        switch (sUriMatcher.get(u)) {
            case 1: // TASKS
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_TASK_ID);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_AUTHOR_NAME);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_SERVER_ID);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_SHORT_NAME);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_DESCRIPTION);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_CREATION_TIME);
                valuePutter(values, TaskContract.Tasks.COLUMN_NAME_RATED);

                rowId = db.insertOrThrow(TaskContract.Tasks.TABLE_NAME, null, values);
                if (rowId > 0) {
                    getContext().getContentResolver().notifyChange(TaskContract.Tasks.CONTENT_URI, null);
                }
                break;

            case 2: // CHECKPOINTS
                valuePutter(values, TaskContract.Checkpoints.COLUMN_NAME_TASK_ID);
                valuePutter(values, TaskContract.Checkpoints.COLUMN_NAME_TEXT);
                valuePutter(values, TaskContract.Checkpoints.COLUMN_NAME_ID);
                valuePutter(values, TaskContract.Checkpoints.COLUMN_NAME_STATE);

                rowId = db.insertOrThrow(TaskContract.Checkpoints.TABLE_NAME, null, values);
                if (rowId > 0) {
                    getContext().getContentResolver().notifyChange(TaskContract.Checkpoints.CONTENT_URI, null);
                }
                break;
        }
        return rowUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    private void valuePutter(ContentValues values, String columnName) {
        if (!values.containsKey(columnName)) {
            values.put(columnName, "");
        }
    }

    private boolean uriChecker(Uri uri) {
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        return !(sUriMatcher.get(u) != TASKS && sUriMatcher.get(u) != CHECKPOINTS);
    }
}
