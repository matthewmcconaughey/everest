package com.mcconaughey.matthew.everest.utils;

import android.content.ContentResolver;
import android.database.Cursor;

import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;

import java.util.ArrayList;

/**
 * Created by dmitri on 28.05.16.
 */
public class SubscribesGetter {
    public static ArrayList<String> getSubscribes(ContentResolver resolver) {
        Cursor cursor = resolver.query(ProfileContract.MySubscribes.CONTENT_URI,
                ProfileContract.MySubscribes.FEED_PROJECTION, null, null, null);
        ArrayList<String> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            result.add(cursor.getString(cursor.getColumnIndex(ProfileContract
                    .MySubscribes.COLUMN_NAME_SERVER_ID)));
        }
        cursor.close();
        return result;
    }
}
