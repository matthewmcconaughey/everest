package com.mcconaughey.matthew.everest.database.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.mcconaughey.matthew.everest.database.DataBaseHelper;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;

import java.util.HashMap;

public class ProfileContentProvider extends ContentProvider {

    private DataBaseHelper dbHelper;
    private Context context;

    private static HashMap<String, String> profileProjection;
    private static HashMap<String, String> mySubscribesProjection;
    private static HashMap<String, String> mySubscribersProjection;
    private static HashMap<String, String> myProfileProjection;

    private static final Integer PROFILE = 1;
    private static final Integer MYSUBSCRIBES = 2;
    private static final Integer MYSUBSCRIBERS = 3;
    private static final Integer SUBSCRIBES = 4;
    private static final Integer MY_PROFILE = 5;

    private static final HashMap<String, Integer> sUriMatcher;
//    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new HashMap<>();
        sUriMatcher.put(ProfileContract.SCHEME + ProfileContract.AUTHORITY + "/profiles", PROFILE);
        sUriMatcher.put(ProfileContract.SCHEME + ProfileContract.AUTHORITY + "/mysubscribes", MYSUBSCRIBES);
        sUriMatcher.put(ProfileContract.SCHEME + ProfileContract.AUTHORITY + "/mysubscribers", MYSUBSCRIBERS);
        sUriMatcher.put(ProfileContract.SCHEME + ProfileContract.AUTHORITY + "/myprofile", MY_PROFILE);

        profileProjection = new HashMap<>();
        for(int i=0; i < ProfileContract.Profiles.FULL_PROFILE_PROJECTION.length; i++) {
            profileProjection.put(
                    ProfileContract.Profiles.FULL_PROFILE_PROJECTION[i],
                    ProfileContract.Profiles.FULL_PROFILE_PROJECTION[i]);
        }

        mySubscribesProjection = new HashMap<>();
        for(int i = 0; i < ProfileContract.MySubscribes.DEFAULT_PROJECTION.length; i++) {
            mySubscribesProjection.put(
                    ProfileContract.MySubscribes.DEFAULT_PROJECTION[i],
                    ProfileContract.MySubscribes.DEFAULT_PROJECTION[i]);
        }

        mySubscribersProjection = new HashMap<>();
        for(int i = 0; i < ProfileContract.MySubscribers.DEFAULT_PROJECTION.length; i++) {
            mySubscribesProjection.put(
                    ProfileContract.MySubscribers.DEFAULT_PROJECTION[i],
                    ProfileContract.MySubscribers.DEFAULT_PROJECTION[i]);
        }

        myProfileProjection = new HashMap<>();
        for(int i=0; i < ProfileContract.MyProfile.DEFAULT_PROJECTION.length; i++) {
            myProfileProjection.put(
                    ProfileContract.MyProfile.DEFAULT_PROJECTION[i],
                    ProfileContract.MyProfile.DEFAULT_PROJECTION[i]);
        }

    }

    public ProfileContentProvider() {

    }

    public ProfileContentProvider(Context context) {
        this.context = context;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        if (!uriChecker(uri)) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        dbHelper = new DataBaseHelper(getContext());

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String orderBy = null;
        switch (sUriMatcher.get(u)) {
            case 1: // PROFILE
                qb.setTables(ProfileContract.Profiles.TABLE_NAME);
                qb.setProjectionMap(profileProjection);
                qb.appendWhere(ProfileContract.Profiles.COLUMN_NAME_SERVER_ID + "= ?");
                break;
            case 2: // MYSUBSCRIBES
                qb.setTables(ProfileContract.MySubscribes.TABLE_NAME);
                qb.setProjectionMap(mySubscribesProjection);
                if (selectionArgs != null)
                    qb.appendWhere(ProfileContract.MySubscribes.COLUMN_NAME_SERVER_ID + "= ?");
                orderBy = ProfileContract.MySubscribes.DEFAULT_ORDER;
                break;
            case 3: // MYSUBSCRIBERS
                qb.setTables(ProfileContract.MySubscribers.TABLE_NAME);
                qb.setProjectionMap(mySubscribesProjection);
                if (selectionArgs != null)
                    qb.appendWhere(ProfileContract.MySubscribers.COLUMN_NAME_SERVER_ID + "= ?");
                orderBy = ProfileContract.MySubscribers.DEFAULT_ORDER;
                break;
            case 4:
                break;
            case 5: // MY_PROFILE
                qb.setTables(ProfileContract.MyProfile.TABLE_NAME);
                qb.setProjectionMap(myProfileProjection);
                break;
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();

        if (!uriChecker(uri)) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        dbHelper = new DataBaseHelper(getContext());
        Context context = getContext();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        }
        else {
            values = new ContentValues();
        }

        long rowId = -1;
        Uri rowUri = Uri.EMPTY;
        switch (sUriMatcher.get(u)) {
            case 1: // PROFILES
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_EMAIL);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_NAME);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_LAST_NAME);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_SERVER_ID);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBES_COUNT);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBERS_COUNT);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_TASKS_COUNT);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_COMPLETED_TASKS_COUNT);
                valuePutter(values, ProfileContract.Profiles.COLUMN_NAME_ABOUT);

                rowId = insertOrUpdateById(db, uri, ProfileContract.Profiles.TABLE_NAME, values);
                if (rowId > 0) {
                    getContext().getContentResolver().notifyChange(ProfileContract.Profiles.CONTENT_URI, null);
                }
                break;

            case 2: // MYSUBSCRIBES
                valuePutter(values, ProfileContract.MySubscribes.COLUMN_NAME_SERVER_ID);
                valuePutter(values, ProfileContract.MySubscribes.COLUMN_NAME_EMAIL);
                valuePutter(values, ProfileContract.MySubscribes.COLUMN_NAME_LAST_NAME);
                valuePutter(values, ProfileContract.MySubscribes.COLUMN_NAME_NAME);

                rowId = insertOrUpdateById(db, uri, ProfileContract.MySubscribes.TABLE_NAME, values);
                if (rowId > 0) {
                    getContext().getContentResolver().notifyChange(ProfileContract.MySubscribes.CONTENT_URI, null);
                }
                break;
            case 3: // MYSUBSCRIBERS
                valuePutter(values, ProfileContract.MySubscribers.COLUMN_NAME_SERVER_ID);
                valuePutter(values, ProfileContract.MySubscribers.COLUMN_NAME_EMAIL);
                valuePutter(values, ProfileContract.MySubscribers.COLUMN_NAME_LAST_NAME);
                valuePutter(values, ProfileContract.MySubscribers.COLUMN_NAME_NAME);

                rowId = insertOrUpdateById(db, uri, ProfileContract.MySubscribers.TABLE_NAME, values);
                if (rowId > 0) {
                    getContext().getContentResolver().notifyChange(ProfileContract.MySubscribers.CONTENT_URI, null);
                }
                break;
            case 4:
                break;
            case 5: // MY_PROFILE
                valuePutter(values, ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN);
                valuePutter(values, ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID);
                valuePutter(values, ProfileContract.MyProfile.COLUMN_NAME_EMAIL);
                valuePutter(values, ProfileContract.MyProfile.COLUMN_NAME_NAME);
                valuePutter(values, ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME);

                rowId = insertOrUpdateById(db, uri, ProfileContract.MyProfile.TABLE_NAME, values);
                if (rowId > 0) {
                    context.getContentResolver().notifyChange(ProfileContract.MyProfile.CONTENT_URI, null);
                }
                break;
        }
        return rowUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String finalWhere;
        int count;
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        switch (sUriMatcher.get(u)) {
            case 2: // MYSUBSCRIBES
                finalWhere = ProfileContract.MySubscribes.COLUMN_NAME_SERVER_ID + " = ?";
                count = db.delete(ProfileContract.MySubscribes.TABLE_NAME, finalWhere, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        if (sUriMatcher.get(u) != PROFILE && sUriMatcher.get(u) != MYSUBSCRIBES &&
                sUriMatcher.get(u) != MYSUBSCRIBERS && sUriMatcher.get(u) != SUBSCRIBES &&
                sUriMatcher.get(u) != MY_PROFILE) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = 0;
        String finalWhere = "";
        String email;
        switch (sUriMatcher.get(u)) {
            case 1: // PROFILES
                finalWhere += ProfileContract.Profiles.COLUMN_NAME_EMAIL + " = ? ";
                if (selection != null) {
                    finalWhere = finalWhere + " AND " + selection;
                }
                count = db.update(ProfileContract.Profiles.TABLE_NAME, values, finalWhere, selectionArgs);
                break;
            case 2: // MYSUBSCRIBES
                finalWhere += ProfileContract.MySubscribes.COLUMN_NAME_EMAIL + " = ? ";
                if (selection !=null) {
                    finalWhere = finalWhere + " AND " + selection;
                }
                count = db.update(ProfileContract.MySubscribes.TABLE_NAME, values, finalWhere, selectionArgs);
                break;
            case 3: // MYSUBSCRIBERS
                finalWhere += ProfileContract.MySubscribers.COLUMN_NAME_EMAIL + " = ? ";
                if (selection !=null) {
                    finalWhere = finalWhere + " AND " + selection;
                }
                count = db.update(ProfileContract.MySubscribers.TABLE_NAME, values, finalWhere, selectionArgs);
                break;
            case 4:
                break;
            case 5: // MY_PROFILE
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    private long insertOrUpdateById(SQLiteDatabase db, Uri uri, String table,
                                    ContentValues values) throws SQLException {
        try {
            return db.insertOrThrow(table, null, values);
        } catch (SQLiteConstraintException e) {
            String[] email = new String[1];
            email[0] = (String) values.get("email");
            values.remove("email");
            update(uri, values, null, email);
            return 1;
        }
    }

    private void valuePutter(ContentValues values, String columnName) {
        if (!values.containsKey(columnName)) {
            values.put(columnName, "");
        }
    }

    private boolean uriChecker(Uri uri) {
        String u = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
        return !(sUriMatcher.get(u) != PROFILE && sUriMatcher.get(u) != MYSUBSCRIBES &&
                sUriMatcher.get(u) != MYSUBSCRIBERS && sUriMatcher.get(u) != SUBSCRIBES &&
                sUriMatcher.get(u) != MY_PROFILE);
    }
}
