package com.mcconaughey.matthew.everest.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by alexander on 18.04.16.
 */

public class NetworkReceiver extends BroadcastReceiver {

    private final EventBus eventBus = EventBus.getDefault();

    @Override
    public void onReceive(Context context, Intent intent) {
        String status = NetworkState.getConnectivityStatusString(context);
        eventBus.post(status);
    }
}