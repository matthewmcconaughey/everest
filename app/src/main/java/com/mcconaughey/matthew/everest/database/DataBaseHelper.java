package com.mcconaughey.matthew.everest.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;
import com.mcconaughey.matthew.everest.network.REST;

public class DataBaseHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "everest.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String UNIQUE_PROPERTY = " not null UNIQUE";


    public static final String SQL_CREATE_TABLE_PROFILES =
            "CREATE TABLE IF NOT EXISTS " + ProfileContract.Profiles.TABLE_NAME + " (" +
                    ProfileContract.Profiles._ID + " INTEGER PRIMARY KEY," +
                    ProfileContract.Profiles.COLUMN_NAME_SERVER_ID + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_EMAIL + TEXT_TYPE + UNIQUE_PROPERTY + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_LAST_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBES_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBERS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_TASKS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_COMPLETED_TASKS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.Profiles.COLUMN_NAME_ABOUT + TEXT_TYPE +
                    " )";

    public static final String SQL_CREATE_TABLE_MY_SUBSCRIBES =
            "CREATE TABLE IF NOT EXISTS " + ProfileContract.MySubscribes.TABLE_NAME + " (" +
                    ProfileContract.MySubscribes._ID + " INTEGER PRIMARY KEY," +
                    ProfileContract.MySubscribes.COLUMN_NAME_SERVER_ID + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MySubscribes.COLUMN_NAME_EMAIL + TEXT_TYPE + UNIQUE_PROPERTY + COMMA_SEP +
                    ProfileContract.MySubscribes.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MySubscribes.COLUMN_NAME_LAST_NAME + TEXT_TYPE +
                    " )";

    public static final String SQL_CREATE_TABLE_MY_SUBSCRIBERS =
            "CREATE TABLE IF NOT EXISTS " + ProfileContract.MySubscribers.TABLE_NAME + " (" +
                    ProfileContract.MySubscribers._ID + " INTEGER PRIMARY KEY," +
                    ProfileContract.MySubscribers.COLUMN_NAME_SERVER_ID + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MySubscribers.COLUMN_NAME_EMAIL + TEXT_TYPE + UNIQUE_PROPERTY + COMMA_SEP +
                    ProfileContract.MySubscribers.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MySubscribers.COLUMN_NAME_LAST_NAME + TEXT_TYPE +
                    " )";

    public static final String SQL_CREATE_TABLE_MY_PROFILE =
            "CREATE TABLE IF NOT EXISTS " + ProfileContract.MyProfile.TABLE_NAME + " (" +
                    ProfileContract.MyProfile._ID + " INTEGER PRIMARY KEY," +
                    ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_SUBSCRIBES_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_SUBSCRIBERS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_TASKS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_COMPLETED_TASKS_COUNT + TEXT_TYPE + COMMA_SEP +
                    ProfileContract.MyProfile.COLUMN_NAME_ABOUT + TEXT_TYPE +
                    " )";

    public static final String SQL_CREATE_TABLE_TASKS =
            "CREATE TABLE IF NOT EXISTS " + TaskContract.Tasks.TABLE_NAME + " (" +
                    TaskContract.Tasks._ID + " INTEGER PRIMARY KEY," +
                    TaskContract.Tasks.COLUMN_NAME_TASK_ID + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_SERVER_ID + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_AUTHOR_NAME + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_SHORT_NAME + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_CREATION_TIME + TEXT_TYPE  + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_RATING + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Tasks.COLUMN_NAME_RATED + TEXT_TYPE +
                    " )";

    public static final String SQL_CREATE_TABLE_CHECKPOINTS =
            "CREATE TABLE IF NOT EXISTS " + TaskContract.Checkpoints.TABLE_NAME + " (" +
                    TaskContract.Checkpoints._ID + " INTEGER PRIMARY KEY," +
                    TaskContract.Checkpoints.COLUMN_NAME_TASK_ID + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Checkpoints.COLUMN_NAME_TEXT + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Checkpoints.COLUMN_NAME_ID + TEXT_TYPE + COMMA_SEP +
                    TaskContract.Checkpoints.COLUMN_NAME_STATE + TEXT_TYPE +
                    " )";

    public static final String SQL_DELETE_TABLE_PROFILES =
            "DROP TABLE IF EXISTS " + ProfileContract.Profiles.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_MY_SUBSCRIBES =
            "DROP TABLE IF EXISTS " + ProfileContract.MySubscribes.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_MY_SUBSCRIBERS =
            "DROP TABLE IF EXISTS " + ProfileContract.MySubscribers.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_MY_PROFILE =
            "DROP TABLE IF EXISTS " + ProfileContract.MyProfile.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_TASKS =
            "DROP TABLE IF EXISTS " + TaskContract.Tasks.TABLE_NAME;

    public static final String SQL_DELETE_TABLE_CHECKPOINTS =
            "DROP TABLE IF EXISTS " + TaskContract.Checkpoints.TABLE_NAME;

    public static final String SQL_TRUNCATE_TABLE_TASKS =
            "DELETE FROM " + TaskContract.Tasks.TABLE_NAME;

    public static final String SQL_TRUNCATE_TABLE_CHECKPOINTS =
            "DELETE FROM " + TaskContract.Checkpoints.TABLE_NAME;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_PROFILES);
        db.execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBES);
        db.execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBERS);
//        db.execSQL(SQL_CREATE_TABLE_FRIENDS);
        db.execSQL(SQL_CREATE_TABLE_MY_PROFILE);
        db.execSQL(SQL_CREATE_TABLE_TASKS);
        db.execSQL(SQL_CREATE_TABLE_CHECKPOINTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TABLE_PROFILES);
        db.execSQL(SQL_DELETE_TABLE_MY_SUBSCRIBES);
        db.execSQL(SQL_DELETE_TABLE_MY_SUBSCRIBERS);
        db.execSQL(SQL_DELETE_TABLE_MY_PROFILE);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_TASKS);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_CHECKPOINTS);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void dropDatabase() {
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_PROFILES);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_MY_SUBSCRIBES);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_MY_SUBSCRIBERS);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_MY_PROFILE);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_TASKS);
        this.getWritableDatabase().execSQL(SQL_DELETE_TABLE_CHECKPOINTS);

        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_PROFILES);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBES);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBERS);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_PROFILE);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_TASKS);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_CHECKPOINTS);
    }

    public void createTables() {
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_PROFILES);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBES);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_SUBSCRIBERS);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_MY_PROFILE);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_TASKS);
        this.getWritableDatabase().execSQL(SQL_CREATE_TABLE_CHECKPOINTS);
    }

    public void clearTasksAndCheckpoints() {
        this.getWritableDatabase().execSQL(SQL_TRUNCATE_TABLE_TASKS);
        this.getWritableDatabase().execSQL(SQL_TRUNCATE_TABLE_CHECKPOINTS);
    }

}
