package com.mcconaughey.matthew.everest.events;

import android.database.Cursor;


public class GetFriendsEvent {
    public Cursor friends;

    public GetFriendsEvent(Cursor friends) {
        this.friends = friends;
    }
}
