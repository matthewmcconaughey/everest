package com.mcconaughey.matthew.everest.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.mcconaughey.matthew.everest.network.ProfileProcessor;
import com.mcconaughey.matthew.everest.network.REST;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alexander on 16.04.16.
 */

public class SignIntentService extends IntentService {
    public final static String ACTION_SIGN_IN = "action.SIGN_IN";
    public final static String ACTION_SIGN_UP = "action.SIGN_UP";

    public final static String EXTRA_EMAIL = "extra.EMAIL";
    public final static String EXTRA_PASSWORD = "extra.PASSWORD";
    public final static String EXTRA_NAME = "extra.NAME";
    public final static String EXTRA_SURNAME = "extra.SURNAME";

    public final static String EXTRA_SIGN_IN_RESULT = "extra.SIGN_IN_RESULT";
    public final static String EXTRA_SIGN_UP_RESULT = "extra.SIGN_UP_RESULT";

    public final static String EXTRA_SIGN_UP_ACCESS_TOKEN = "extra.SIGN_UP_ACCESS_TOKEN";
    public final static String EXTRA_SIGN_UP_SERVER_ID = "extra.SIGN_UP_SERVER_ID";
    public final static String EXTRA_SIGN_UP_EMAIL = "extra.SIGN_UP_EMAIL";
    public final static String EXTRA_SIGN_UP_NAME = "extra.SIGN_UP_NAME";
    public final static String EXTRA_SIGN_UP_LAST_NAME = "extra.SIGN_UP_LAST_NAME";

    private REST rest;

    public SignIntentService() {
        super("SignInService");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        rest = retrofit.create(REST.class);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SIGN_IN.equals(action)) {
                handleSignInAction(intent.getStringExtra(EXTRA_EMAIL),
                        intent.getStringExtra(EXTRA_PASSWORD));
            } else {
                handleSignUpAction(intent.getStringExtra(EXTRA_EMAIL),
                        intent.getStringExtra(EXTRA_PASSWORD),
                        intent.getStringExtra(EXTRA_NAME),
                        intent.getStringExtra(EXTRA_SURNAME));
            }
        }
    }

    private void handleSignInAction(String email, String password) {
        String resp = ProfileProcessor.processMyProfile(getContentResolver(), email, password);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(
                new Intent(ACTION_SIGN_IN).putExtra(EXTRA_SIGN_IN_RESULT, resp)
        );

    }

    private void handleSignUpAction(String email, String password, String name, String surname) {
        Call<REST.SignUpResult> call = rest.signUp(new REST.SignUpBody(email, password, name, surname));

        Response<REST.SignUpResult> result = null;

        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String respAccessToken = "error";
        String respServerID = "error";
        String respEmail = "error";
        String respName = "error";
        String respLastName = "error";
        if (result.isSuccess()) {
            if (!result.body().accessToken.isEmpty()) {
                respAccessToken = result.body().accessToken;
                respServerID = result.body().serverID;
                respEmail = result.body().email;
                respName = result.body().name;
                respLastName = result.body().lastName;
            }
        }

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(
                new Intent(ACTION_SIGN_UP)
                        .putExtra(EXTRA_SIGN_UP_ACCESS_TOKEN, respAccessToken)
                        .putExtra(EXTRA_SIGN_UP_SERVER_ID, respServerID)
                        .putExtra(EXTRA_SIGN_UP_EMAIL, respEmail)
                        .putExtra(EXTRA_SIGN_UP_NAME, respName)
                        .putExtra(EXTRA_SIGN_UP_LAST_NAME, respLastName)

        );

    }

}