package com.mcconaughey.matthew.everest.fragments.feed.utils;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcconaughey.matthew.everest.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alexander on 13.03.16.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {

    List<FeedItem> feedItems;
    private AdapterCallback itemTouchedCallback;

    public FeedAdapter(List<FeedItem> feedItems, Context context){
        this.feedItems = feedItems;
        if (context instanceof AdapterCallback)
            this.itemTouchedCallback = ((AdapterCallback) context);
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_item, viewGroup, false);
        return new FeedViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final FeedViewHolder feedViewHolder, int i) {
        feedViewHolder.userName.setText(this.feedItems.get(i).userName);
        feedViewHolder.shortDescription.setText(this.feedItems.get(i).shortDescription);
        feedViewHolder.rating.setText(this.feedItems.get(i).rating);
        feedViewHolder.date.setText(this.feedItems.get(i).date);
        feedViewHolder.userAvatar.setImageDrawable(this.feedItems.get(i).userAvatar);
        feedViewHolder.taskID = this.feedItems.get(i).taskID;
        feedViewHolder.description = this.feedItems.get(i).description;

        if (this.feedItems.get(i).rated.equals("0")) {
            feedViewHolder.rated.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
            feedViewHolder.rated.setAlpha((float)0.4);
            feedViewHolder.rating.setAlpha((float)0.4);
        } else {
            feedViewHolder.rated.setImageResource(R.drawable.ic_favorite_pressed_blue_24dp);
            feedViewHolder.rated.setAlpha((float)1);
            feedViewHolder.rating.setAlpha((float)1);
        }

        feedViewHolder.likeButtonLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                itemTouchedCallback.onFeedItemLikeTryCallback(feedViewHolder.taskID, feedViewHolder.getItemId());
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public int getItemCount() {
        return this.feedItems.size();
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;

        public View view;

        TextView userName;
        TextView date;
        TextView shortDescription;
        String description;
        TextView rating;
        CircleImageView userAvatar;
        String taskID;
        ImageButton rated;

        LinearLayout likeButtonLayout;

        FeedViewHolder(View itemView) {

            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.feed_item);
            likeButtonLayout = (LinearLayout) itemView.findViewById(R.id.likeButtonLayout);
            userName = (TextView)itemView.findViewById(R.id.userNameTextView);
            date = (TextView)itemView.findViewById(R.id.dateTextView);
            shortDescription = (TextView)itemView.findViewById(R.id.shortDescriptionTextView);
            rating = (TextView)itemView.findViewById(R.id.ratingTextView);
            rated = (ImageButton)itemView.findViewById(R.id.likeButton);
            userAvatar = (CircleImageView)itemView.findViewById(R.id.user_avatar);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    itemTouchedCallback.onFeedItemTouchedCallback(taskID, userName.getText().toString(),
                            shortDescription.getText().toString(), description, date.getText().toString(),
                            rating.getText().toString());
                }
            });

        }


    }

    public interface AdapterCallback {
        void onFeedItemTouchedCallback(String taskID, String userName, String shortDescription,
                                       String description, String date, String rating);

        void onFeedItemLikeTryCallback(String taskID, long itemId);
    }
}

