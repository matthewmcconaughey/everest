package com.mcconaughey.matthew.everest.database.contracts;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by dmitri on 28.05.16.
 */
public class TaskContract {
    private TaskContract() {}
    public static final String AUTHORITY = "com.mcconaughey.matthew.everest.database.providers.TaskContentProvider";
    public static final String SCHEME = "content://";

    public static abstract class Tasks implements BaseColumns {

        public static final String TABLE_NAME = "tasks";
        private static final String PATH_TASKS = "/tasks";
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_TASKS);

        public static final String COLUMN_NAME_TASK_ID = "taskID";
        public static final String COLUMN_NAME_AUTHOR_NAME = "authorName";
        public static final String COLUMN_NAME_SERVER_ID = "serverID";
        public static final String COLUMN_NAME_SHORT_NAME = "shortName";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_CREATION_TIME = "creationTime";
        public static final String COLUMN_NAME_RATING = "rating";
        public static final String COLUMN_NAME_RATED = "rated";


        public static final String[] DEFAULT_PROJECTION = new String[] {
                Tasks._ID,
                Tasks.COLUMN_NAME_TASK_ID,
                Tasks.COLUMN_NAME_AUTHOR_NAME,
                Tasks.COLUMN_NAME_SERVER_ID,
                Tasks.COLUMN_NAME_SHORT_NAME,
                Tasks.COLUMN_NAME_DESCRIPTION,
                Tasks.COLUMN_NAME_CREATION_TIME,
                Tasks.COLUMN_NAME_RATING,
                Tasks.COLUMN_NAME_RATED,
        };
    }

    public static abstract class Checkpoints implements BaseColumns {

        public static final String TABLE_NAME = "checkpoints";
        private static final String PATH_CHECKPOINTS = "/checkpoints";
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_CHECKPOINTS);

        public static final String COLUMN_NAME_TASK_ID = "taskID";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_STATE = "state";


        public static final String[] DEFAULT_PROJECTION = new String[] {
                Checkpoints._ID,
                Checkpoints.COLUMN_NAME_TASK_ID,
                Checkpoints.COLUMN_NAME_TEXT,
                Checkpoints.COLUMN_NAME_ID,
                Checkpoints.COLUMN_NAME_STATE,
        };
    }
}
