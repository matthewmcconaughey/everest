package com.mcconaughey.matthew.everest.events;

import android.database.Cursor;

/**
 * Created by dmitri on 29.05.16.
 */
public class FeedReceivedAndSavedEvent {
    public Cursor feed;

    public FeedReceivedAndSavedEvent(Cursor feed) {
        this.feed = feed;
    }
}
