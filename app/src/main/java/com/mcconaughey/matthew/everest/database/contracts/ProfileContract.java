package com.mcconaughey.matthew.everest.database.contracts;

import android.net.Uri;
import android.provider.BaseColumns;

public class ProfileContract {

    private ProfileContract() {}
    public static final String AUTHORITY = "com.mcconaughey.matthew.everest.database.providers.ProfileContentProvider";
    public static final String SCHEME = "content://";

    public static abstract class Profiles implements BaseColumns {

        public static final String TABLE_NAME = "profiles";
        private static final String PATH_PROFILES = "/profiles";
        private static final String PATH_PROFILES_ID = "/profiles/";
        public static final int PROFILES_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_PROFILES);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_PROFILES_ID);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.profiles";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.profiles";
        public static final String DEFAULT_SORT_ORDER = "lastName ASC";

        public static final String COLUMN_NAME_SERVER_ID = "serverID";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_SUBSCRIBES_COUNT = "subscribesCount";
        public static final String COLUMN_NAME_SUBSCRIBERS_COUNT = "subscribersCount";
        public static final String COLUMN_NAME_TASKS_COUNT = "tasksCount";
        public static final String COLUMN_NAME_COMPLETED_TASKS_COUNT = "completedTasksCount";
        public static final String COLUMN_NAME_ABOUT = "about";


        public static final String[] DEFAULT_PROJECTION = new String[] {
                Profiles._ID,
                Profiles.COLUMN_NAME_SERVER_ID,
                Profiles.COLUMN_NAME_EMAIL,
                Profiles.COLUMN_NAME_NAME,
                Profiles.COLUMN_NAME_LAST_NAME,
        };

        public static final String[] FULL_PROFILE_PROJECTION = new String[] {
                Profiles._ID,
                Profiles.COLUMN_NAME_SERVER_ID,
                Profiles.COLUMN_NAME_EMAIL,
                Profiles.COLUMN_NAME_NAME,
                Profiles.COLUMN_NAME_LAST_NAME,
                Profiles.COLUMN_NAME_SUBSCRIBES_COUNT,
                Profiles.COLUMN_NAME_SUBSCRIBERS_COUNT,
                Profiles.COLUMN_NAME_TASKS_COUNT,
                Profiles.COLUMN_NAME_COMPLETED_TASKS_COUNT,
                Profiles.COLUMN_NAME_ABOUT,
        };
    }

    public static abstract class MySubscribes implements BaseColumns {
        public static final String TABLE_NAME = "mysubscribes";

        private static final String PATH_MYSUBSCRIBES = "/mysubscribes";
        public static final int SUBSCRIBES_ID_PATH_POSITION = 3;
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_MYSUBSCRIBES);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.mysubscribes";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.mysubscribes";

        public static final String DEFAULT_ORDER = "serverID";

        public static final String COLUMN_NAME_SERVER_ID = "serverID";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";

        public static final String[] DEFAULT_PROJECTION = new String[] {
                MySubscribes._ID,
                MySubscribes.COLUMN_NAME_SERVER_ID,
                MySubscribes.COLUMN_NAME_EMAIL,
                MySubscribes.COLUMN_NAME_NAME,
                MySubscribes.COLUMN_NAME_LAST_NAME,
        };

        public static final String[] FEED_PROJECTION = new String[] {
                MySubscribes.COLUMN_NAME_SERVER_ID,
        };
    }

    public static abstract class MySubscribers implements BaseColumns {
        public static final String TABLE_NAME = "mysubscribers";

        private static final String PATH_MYSUBSCRIBERS = "/mysubscribers";
        public static final int SUBSCRIBERS_ID_PATH_POSITION = 3;
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_MYSUBSCRIBERS);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.mysubscribers";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.mysubscribers";

        public static final String DEFAULT_ORDER = "serverID";

        public static final String COLUMN_NAME_SERVER_ID = "serverID";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";

        public static final String[] DEFAULT_PROJECTION = new String[] {
                MySubscribers._ID,
                MySubscribers.COLUMN_NAME_SERVER_ID,
                MySubscribers.COLUMN_NAME_EMAIL,
                MySubscribers.COLUMN_NAME_NAME,
                MySubscribers.COLUMN_NAME_LAST_NAME,
        };
    }

    public static abstract class MyProfile implements BaseColumns {
        public static final String TABLE_NAME = "myprofile";

        private static final String PATH_MYPROFILE = "/myprofile";
        private static final String PATH_MYPROFILE_ID = "/myprofile/#";
        public static final int MYPROFILE_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI =  Uri.parse(SCHEME + AUTHORITY + PATH_MYPROFILE);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_MYPROFILE_ID);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.myprofile";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.myprofile";

        public static final String COLUMN_NAME_ACCESS_TOKEN = "accessToken";
        public static final String COLUMN_NAME_SERVER_ID = "serverID";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_SUBSCRIBES_COUNT = "subscribesCount";
        public static final String COLUMN_NAME_SUBSCRIBERS_COUNT = "subscribersCount";
        public static final String COLUMN_NAME_TASKS_COUNT = "tasksCount";
        public static final String COLUMN_NAME_COMPLETED_TASKS_COUNT = "completedTasksCount";
        public static final String COLUMN_NAME_ABOUT = "about";

        public static final String[] DEFAULT_PROJECTION = new String[] {
                MyProfile._ID,
                MyProfile.COLUMN_NAME_ACCESS_TOKEN,
                MyProfile.COLUMN_NAME_SERVER_ID,
                MyProfile.COLUMN_NAME_EMAIL,
                MyProfile.COLUMN_NAME_NAME,
                MyProfile.COLUMN_NAME_LAST_NAME,
        };

        public static final String[] ACCESS_TOKEN_PROJECTION = new String[] {
                MyProfile.COLUMN_NAME_ACCESS_TOKEN,
        };

        public static final String[] SERVER_ID_PROJECTION = new String[] {
                MyProfile.COLUMN_NAME_SERVER_ID,
        };
    }
}
