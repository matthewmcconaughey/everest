package com.mcconaughey.matthew.everest.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.database.DataBaseHelper;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;
import com.mcconaughey.matthew.everest.events.CheckpointsLoaded;
import com.mcconaughey.matthew.everest.events.FeedReceivedAndSavedEvent;
import com.mcconaughey.matthew.everest.events.GetFriendsEvent;
import com.mcconaughey.matthew.everest.events.OpenTaskEvent;
import com.mcconaughey.matthew.everest.fragments.addtask.AddTaskFragment;
import com.mcconaughey.matthew.everest.fragments.feed.FeedFragment;
import com.mcconaughey.matthew.everest.fragments.friends.FriendsFragment;
import com.mcconaughey.matthew.everest.fragments.SettingsFragment;
import com.mcconaughey.matthew.everest.fragments.TaskFragment;
import com.mcconaughey.matthew.everest.fragments.addtask.data.AbstractDataProvider;
import com.mcconaughey.matthew.everest.fragments.addtask.data.CheckPointsDataProvider;
import com.mcconaughey.matthew.everest.fragments.feed.utils.FeedAdapter;
import com.mcconaughey.matthew.everest.fragments.friends.utils.FriendsAdapter;
import com.mcconaughey.matthew.everest.network.ServiceHelper;
import com.mcconaughey.matthew.everest.utils.MyProfileGetter;
import com.mcconaughey.matthew.everest.utils.NetworkState;
import com.mcconaughey.matthew.everest.utils.AccessTokenGetter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import static android.support.design.widget.Snackbar.LENGTH_LONG;


// TODO: показать юзер таскс на приход интента с соответствующей строчкой

public class MainActivity extends AppCompatActivity  implements FeedAdapter.AdapterCallback, FriendsAdapter.AdapterCallback,
        LoaderManager.LoaderCallbacks<Cursor> {

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    Fragment feedFragment;
    Fragment settingsFragment;
    Fragment taskFragment;
    Fragment userTasksFragment;
    Fragment myFriendsFragment;
    Fragment addTaskFragment;

    private String INTERNET_CONNECTION_STATE = "";
    private ProgressBar progressBar;

    public android.support.design.widget.CoordinatorLayout coordinatorLayout;

    Drawer drawer;

    public FloatingActionButton fab;
    ViewGroup viewGroup;

    public static final int MY_SUBSCRIBES_LOADER = 0;
    public static final int MY_PROFILE_LOADER = 1;
    public static final int MY_SUBSCRIBERS_LOADER = 2;
    public static final int FEED_LOADER = 3;
    public static final int CHECKPOINTS_LOADER = 4;
    private EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.fragmentManager = getFragmentManager();

        this.initFloatingActionButton();

        this.initDrawer(savedInstanceState);
        progressBar = (ProgressBar) this.findViewById(R.id.progress_bar);

        feedFragment = new FeedFragment();
        Intent intent = getIntent();
        if (intent != null && intent.getStringExtra("fragment") != null) {
            if (intent.getStringExtra("fragment").equals("UserTasks")) {
                // TODO: чё-нить сделать с пользовательской лентой
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragmentContainer, feedFragment);
        transaction.commit();

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.mainCoordinator);

        this.INTERNET_CONNECTION_STATE = getInitialConnectionType();

        eventBus.register(this);
    }

    public String getInitialConnectionType(){
        return NetworkState.getConnectivityStatusString(this);
    }

    @Override
    public void onFeedItemTouchedCallback(String taskID, String userName, String shortDescription,
                                          String description, String date, String rating) {

        EventBus.getDefault().postSticky(new OpenTaskEvent(taskID, userName, shortDescription, description,
                date, rating));
        switchFragments("Task");

    }

    @Override
    public void onFeedItemLikeTryCallback(String taskID, long itemId) {
        MyProfileGetter.Profile myProfile = MyProfileGetter.getMyProfile(getContentResolver());
        //serverId - мой id
        //taskId - айдишник таска на сервере
        ServiceHelper.rate(this, myProfile.serverID, myProfile.accessToken, taskID, itemId);
    }

    @Override
    public void onFriendsItemTouchedCallback(String name, String serverID) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("Username", name);
        intent.putExtra("serverID", serverID);
        String accessToken = AccessTokenGetter.getAccessToken(getContentResolver());
        intent.putExtra("accessToken", accessToken);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    drawer.openDrawer();
                    break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void hideFloatingActionButton(){
        viewGroup.removeView(fab);
    }

    public void showFloatingActionButton(){
        if (findViewById(R.id.fab) == null)
            viewGroup.addView(fab);
    }

    private void initFloatingActionButton() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragments("AddTaskFragment");
            }
        });
        viewGroup = (ViewGroup)(fab.getParent());
    }

    private void initDrawer(Bundle savedInstanceState) {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        Bitmap profileIcon = BitmapFactory.decodeResource(getResources(), R.drawable.profile);

        String name = "Ватрушкин Николай";
        String email = "vatrushka@mail.ru";
        try {
            DataBaseHelper db = new DataBaseHelper(this);
            db.createTables();
            Cursor cursor = getContentResolver().query(ProfileContract.MyProfile.CONTENT_URI,
                    ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
            cursor.moveToFirst();
            name = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_NAME)) + " " +
                    cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME));
            email = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_EMAIL));
        } catch (CursorIndexOutOfBoundsException e) {
            Log.d("User is not logged!", "");
        }

        final IProfile profile = new ProfileDrawerItem().withName(name)
                .withEmail(email)
                .withIcon(profileIcon);


        int size = 0;
        if (profileIcon.getWidth() > profileIcon.getHeight()) {
            size = profileIcon.getHeight();
        } else {
            size = profileIcon.getWidth();
        }
        profile.getIcon().setBitmap(Bitmap.createScaledBitmap(profileIcon, size, size, false));

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withSelectionListEnabledForSingleProfile(false)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        profile
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        Cursor cursor = getContentResolver().query(ProfileContract.MyProfile.CONTENT_URI,
                                ProfileContract.MyProfile.DEFAULT_PROJECTION, null, null, null);
                        cursor.moveToFirst();
                        String name = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_NAME)) +
                                " " + cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME));
                        intent.putExtra("Username", name);
                        String serverID = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID));
                        intent.putExtra("serverID", serverID);
                        cursor.moveToFirst();
                        String accessToken = cursor.getString(cursor.getColumnIndex(ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN));
                        intent.putExtra("accessToken", accessToken);
                        intent.putExtra("profile", ProfileActivity.MY_PROFILE_STATE);
                        startActivity(intent);
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withHeightDp(200)
                .build();

        //Create the drawer
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Лента").withIcon(FontAwesome.Icon.faw_comments_o).withIdentifier(1),
//                        new PrimaryDrawerItem().withName("Мои цели").withIcon(FontAwesome.Icon.faw_check_square_o).withIdentifier(2),
                        new PrimaryDrawerItem().withName("Мои подписки").withIcon(FontAwesome.Icon.faw_users).withIdentifier(3),
                        new PrimaryDrawerItem().withName("Мои подписчики").withIcon(FontAwesome.Icon.faw_user_plus).withIdentifier(7),
                        new PrimaryDrawerItem().withName("Поиск людей").withIcon(FontAwesome.Icon.faw_search).withIdentifier(6),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("Выход").withIcon(FontAwesome.Icon.faw_sign_out).withIdentifier(5)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch ((int) drawerItem.getIdentifier()) {
                            case 1:
                                if (feedFragment == null) {
                                    feedFragment = new SettingsFragment();
                                }
                                if(!feedFragment.isVisible())
                                    switchFragments("Feed");
                                break;
                            case 2:
                                break;
                            case 3:
                                if (myFriendsFragment == null) {
                                    myFriendsFragment = new FriendsFragment();
                                }
                                ((FriendsFragment) myFriendsFragment).initDbVariables(FriendsFragment.SUBSCRIBES);
                                getSupportActionBar().setTitle("Мои подписки");
                                if (!myFriendsFragment.isVisible()) {
                                    switchFragments("MySubscribes");
                                } else {
                                    ((FriendsFragment) myFriendsFragment).reload();
                                }
                                break;
                            case 4:
                                if (settingsFragment == null) {
                                    settingsFragment = new SettingsFragment();
                                }
                                if (!settingsFragment.isVisible())
                                    switchFragments("Settings");
                                break;
                            case 5:
                                DataBaseHelper dataBaseHelper = new DataBaseHelper(MainActivity.this);
                                dataBaseHelper.dropDatabase();
                                Intent intent = new Intent(MainActivity.this, SignActivity.class);
                                startActivity(intent);
                                break;
                            case 6:
                                intent = new Intent(MainActivity.this, SearchActivity.class);
                                startActivity(intent);
                                break;
                            case 7:
                                if (myFriendsFragment == null) {
                                    myFriendsFragment = new FriendsFragment();
                                }
                                getSupportActionBar().setTitle("Мои подписчики");
                                ((FriendsFragment) myFriendsFragment).initDbVariables(FriendsFragment.SUBSCRIBERS);
                                getSupportActionBar().setTitle("Мои подписчики");
                                if (!myFriendsFragment.isVisible()) {
                                    switchFragments("MySubscribes");
                                } else {
                                    ((FriendsFragment) myFriendsFragment).reload();
                                }
                                break;
                        }
                        drawer.deselect();
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
        drawer.deselect();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ){
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void switchFragments(String fragmentName) {
        transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        if (!fragmentName.equals("Feed"))
            hideFloatingActionButton();
        else {
            showFloatingActionButton();
        }

        switch (fragmentName) {
            case "Feed":
                transaction.replace(R.id.fragmentContainer, feedFragment);
                // clear back stack
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
            case "Task":
                if (taskFragment == null)
                    taskFragment = new TaskFragment();
                transaction.replace(R.id.fragmentContainer, taskFragment);
                transaction.addToBackStack(null);
                break;
            case "MySubscribes":
                transaction.replace(R.id.fragmentContainer, myFriendsFragment);
                transaction.addToBackStack(null);
                break;
            case "Settings":
                transaction.replace(R.id.fragmentContainer, settingsFragment);
                transaction.addToBackStack(null);
                break;
            case "UserTasks":
                if (userTasksFragment == null)
                    userTasksFragment = new FeedFragment();
                transaction.replace(R.id.fragmentContainer, userTasksFragment);
                transaction.addToBackStack(null);
                break;
            case "AddTaskFragment":
                if (addTaskFragment == null)
                    addTaskFragment = new AddTaskFragment();
                transaction.replace(R.id.fragmentContainer, addTaskFragment);
                transaction.addToBackStack(null);
                break;
            default:
                break;
        }
        transaction.commit();
    }


    private static final String FRAGMENT_LIST_VIEW = "list view";

    /**
     * This method will be called when a list item is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemClicked(int position) {
        final Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_LIST_VIEW);
        AbstractDataProvider.Data data = getDataProvider().getItem(position);

        if (data.isPinned()) {
            // unpin if tapped the pinned item
            data.setPinned(false);
            ((AddTaskFragment) fragment).notifyItemChanged(position);
        }
    }

    private void onItemUndoActionClicked() {
        int position = getDataProvider().undoLastRemoval();
        if (position >= 0) {
            final Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_LIST_VIEW);
            ((AddTaskFragment) fragment).notifyItemInserted(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        eventBus.unregister(this);
    }

    public ProgressBar getProgressBar(){
        return this.progressBar;
    }

    @Subscribe
    public void onEvent(String event) {

        if (!Objects.equals(event, this.INTERNET_CONNECTION_STATE)){

            this.INTERNET_CONNECTION_STATE = event;
            String message = "";

            switch (event) {
                case "WIFI":
                    message = "Подключен к wifi точке";
                    break;
                case "MOBILE_INTERNET":
                    message = "Включен мобильный интернет";
                    break;
                case "NO_CONNECTION":
                    message = "Проверьте соединение с интернетом";
                    break;
                default:
                    break;
            }

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, message, LENGTH_LONG);
            snackbar.show();
        }

    }

    public String getConnectionState(){
        return this.INTERNET_CONNECTION_STATE;
    }


    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";

    public AbstractDataProvider getDataProvider() {
        return new CheckPointsDataProvider();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTACT_URI = null;
        switch (id) {
            case MY_SUBSCRIBES_LOADER:
                CONTACT_URI = ProfileContract.MySubscribes.CONTENT_URI;
                break;
            case MY_PROFILE_LOADER:
                CONTACT_URI = ProfileContract.MyProfile.CONTENT_URI;
                break;
            case MY_SUBSCRIBERS_LOADER:
                CONTACT_URI = ProfileContract.MySubscribers.CONTENT_URI;
                break;
            case FEED_LOADER:
                CONTACT_URI = TaskContract.Tasks.CONTENT_URI;
                break;
            case CHECKPOINTS_LOADER:
                CONTACT_URI = TaskContract.Checkpoints.CONTENT_URI;
                break;
        }
        return new CursorLoader(this, CONTACT_URI, null,
                null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null) {
            switch (loader.getId()) {
                case MY_SUBSCRIBES_LOADER:
                    // здеся курсор с подписками
                    if(cursor.moveToFirst()) {
                        EventBus.getDefault().post(new GetFriendsEvent(cursor));
                    }
                    break;
                case MY_SUBSCRIBERS_LOADER:
                    // здеся курсор с подписонами
                    if(cursor.moveToFirst()) {
                        EventBus.getDefault().post(new GetFriendsEvent(cursor));
                    }
                    break;
                case FEED_LOADER:
                    // здеся курсор с лентой
//                    if(cursor.moveToFirst()) {
                        EventBus.getDefault().post(new FeedReceivedAndSavedEvent(cursor));
//                    }
                    break;
                case CHECKPOINTS_LOADER:
                    // здеся курсор с лентой
                    if(cursor.moveToFirst()) {
                        EventBus.getDefault().post(new CheckpointsLoaded(cursor));
                    }
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}





