package com.mcconaughey.matthew.everest.network;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.activities.MainActivity;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.fragments.friends.FriendsFragment;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dmitri on 18.04.16.
 */
public class ProfileProcessor {


    public static void processGetSubscribes(ContentResolver resolver, String accessToken, String serverID, int mode) {
        REST.Profile[] result = makeGetSubscribesRequest(accessToken, serverID, mode);

        Uri uri = null;
        switch (mode) {
            case MainActivity.MY_SUBSCRIBES_LOADER:
                uri = ProfileContract.MySubscribes.CONTENT_URI;
                break;
            case MainActivity.MY_SUBSCRIBERS_LOADER:
                uri = ProfileContract.MySubscribers.CONTENT_URI;
                break;
        }

        if (result != null) {
            for (int i = 0; i < result.length; i++) {
                ContentValues values = putProfileContentValues(result[i].serverID, result[i].email,
                        result[i].name, result[i].lastName, null, null, null, null, null);
                resolver.insert(uri, values);
            }
        }
    }

    private static REST.Profile[] makeGetSubscribesRequest(String accessToken, String serverID, int mode) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.GetSubscribesResult> call;
        if (mode == MainActivity.MY_SUBSCRIBERS_LOADER) {
            call = rest.getSubscribes(new REST.ProfileRequestBody(accessToken, serverID));
        } else {
            call = rest.getSubscribers(new REST.ProfileRequestBody(accessToken, serverID));
        }

        Response<REST.GetSubscribesResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result != null) {
            return result.body().subscribes;
        } else {
            return null;
        }
    }

    public static void processProfile(ContentResolver resolver, String accessToken, String serverID) {
        REST.Profile result = makeProfileRequest(accessToken, serverID);
        ContentValues values = putProfileContentValues(result.serverID, result.email,
                result.name, result.lastName, result.subscribesCount, result.subscribersCount,
                result.tasksCount, result.completedTasksCount, result.about);
        resolver.insert(ProfileContract.Profiles.CONTENT_URI, values);
    }

    public static String processMyProfile(ContentResolver resolver, String email, String password) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.SignUpResult> call = rest.signIn(new REST.SignInBody(email, password));

        Response<REST.SignUpResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(String.valueOf(result));

        if (result != null && result.isSuccess()) {
            ContentValues values = putProfileContentValues(result.body().serverID, result.body().email,
                    result.body().name, result.body().lastName, null, null, null, null, null);
            values.put("accessToken", result.body().accessToken);
            resolver.insert(ProfileContract.MyProfile.CONTENT_URI, values);
            return result.body().accessToken;
        } else {
            return "error";
        }
    }

    public static String processUpdateProfile(ContentResolver resolver, String accessToken,
                                            String field, String value) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Response<REST.UpdateResult> result = null;
        Call<REST.UpdateResult> call = null;
        int fieldSwitch = Integer.valueOf(field);
        switch (fieldSwitch) {
            case R.id.aboutTextView:
                call = rest.updateAbout(new REST.UpdateAboutBody(accessToken, value));
                field = "about";
                break;
            case R.id.emailTextView:
                call = rest.updateEmail(new REST.UpdateEmailBody(accessToken, value));
                field = "email";
                break;
        }

        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result.isSuccess()) {
            ContentValues values = new ContentValues();
            values.put(String.valueOf(field), value);
            resolver.insert(ProfileContract.MyProfile.CONTENT_URI, values);
            return "OK";
        } else {
            return "error";
        }
    }

    public static REST.Profile[] processFindPeople(ContentResolver resolver, String accessToken,
                                                 String query) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.FindPeopleResult> call = rest.findPeople(new REST.FindPeopleBody(accessToken, query));

        Response<REST.FindPeopleResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result.isSuccess()) {
            return result.body().people;
        } else {
            return null;
        }
    }

    public static boolean processSubscribe(ContentResolver resolver, String accessToken,
                                        String myServerID, String notMyServerID) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.Profile> call = rest.subscribe(new REST.SubscribeBody(accessToken, myServerID, notMyServerID));

        Response<REST.Profile> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert result != null;
        if (result.isSuccess()) {
            resolver.insert(ProfileContract.MySubscribes.CONTENT_URI, putProfileContentValues(result.body().serverID,
                    result.body().email, result.body().name, result.body().lastName, null, null, null, null, null));
//                    result.body().subscribesCount, result.body().subscribersCount,
//                    result.body().tasksCount, result.body().completedTasksCount,
//                    result.body().about));
        }
        return result.isSuccess();
    }

    public static boolean processUnsubscribe(ContentResolver resolver, String accessToken,
                                           String myServerID, String notMyServerID) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.UnubscribeResult> call = rest.unsubscribe(new REST.SubscribeBody(accessToken, myServerID, notMyServerID));

        Response<REST.UnubscribeResult> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert result != null;
        if (result.body().result) {
            String[] selectionArgs = new String[1];
            selectionArgs[0] = notMyServerID;
            resolver.delete(ProfileContract.MySubscribes.CONTENT_URI, null, selectionArgs);
        }
        return result.isSuccess();
    }

    private static REST.Profile makeProfileRequest(String accessToken, String serverID) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(REST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        REST rest = retrofit.create(REST.class);

        Call<REST.Profile> call = rest.getProfile(new REST.ProfileRequestBody(accessToken, serverID));

        Response<REST.Profile> result = null;
        try {
            result = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert result != null;
        return result.body();
    }

    private static ContentValues putProfileContentValues(String serverID, String email, String name, String lastName,
                                                         String subscribesCount, String subscribersCount,
                                                         String tasksCount, String completedTasksCount,
                                                         String about) {
        ContentValues values = new ContentValues();
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_SERVER_ID, serverID);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_EMAIL, email);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_NAME, name);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_LAST_NAME, lastName);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBES_COUNT, subscribesCount);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_SUBSCRIBERS_COUNT, subscribersCount);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_TASKS_COUNT, tasksCount);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_COMPLETED_TASKS_COUNT, completedTasksCount);
        putValueIfNotNull(values, ProfileContract.Profiles.COLUMN_NAME_ABOUT, about);
        return values;
    }

    private static void putValueIfNotNull(ContentValues values, String columnName, String value) {
        if (value != null) {
            values.put(columnName, value);
        }
    }

}
