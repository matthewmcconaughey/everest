package com.mcconaughey.matthew.everest.events;

import android.database.Cursor;

/**
 * Created by dmitri on 21.04.16.
 */
public class GetProfileEvent {
    public Cursor profile;

    public GetProfileEvent(Cursor profile) {
        this.profile = profile;
    }
}
