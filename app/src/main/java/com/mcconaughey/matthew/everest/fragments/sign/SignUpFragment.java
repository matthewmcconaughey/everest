package com.mcconaughey.matthew.everest.fragments.sign;

import android.content.ContentValues;
import android.support.annotation.Nullable;

import com.mcconaughey.matthew.everest.activities.SignActivity;

import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View.OnClickListener;

import com.mcconaughey.matthew.everest.R;
import com.mcconaughey.matthew.everest.database.contracts.ProfileContract;
import com.mcconaughey.matthew.everest.events.SignUpEvent;
import com.mcconaughey.matthew.everest.network.ServiceHelper;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpFragment extends Fragment  {

    private String email;
    private String name;
    private String lastName;

    FrameLayout signUpContainer;

    public SignUpFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        View signIn = getActivity().findViewById(R.id.link_signin);
        signIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 07.04.16 Попросить активити переключить, а не жестко самому
                ((SignActivity) getActivity()).switchFragments("SignIn");
            }
        });

        setSignUpOnClickListener();

        signUpContainer = (FrameLayout) getView().findViewById(R.id.sign_up_container);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onSignUpEvent(SignUpEvent event) {
        // TODO: сохранить токен
        Log.d("Token received: ", event.accessToken);

        ContentValues values = new ContentValues();
        values.put(ProfileContract.MyProfile.COLUMN_NAME_ACCESS_TOKEN, event.accessToken);
        values.put(ProfileContract.MyProfile.COLUMN_NAME_SERVER_ID, event.serverID);
        values.put(ProfileContract.MyProfile.COLUMN_NAME_EMAIL, event.email);
        values.put(ProfileContract.MyProfile.COLUMN_NAME_NAME, event.name);
        values.put(ProfileContract.MyProfile.COLUMN_NAME_LAST_NAME, event.lastName);

        ((SignActivity) getActivity()).getContentResolver().insert(ProfileContract.MyProfile.CONTENT_URI, values);
        ((SignActivity) getActivity()).switchFragments("SignIn");
    }

    public void setSignUpOnClickListener() {
        final View view = getView();
        Button loginBtn = (Button) view.findViewById(R.id.btn_signup);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = ((EditText) view.findViewById(R.id.input_email))
                        .getText().toString();
                String password = ((EditText) view.findViewById(R.id.input_password))
                        .getText().toString();

                name = ((EditText) view.findViewById(R.id.input_name))
                        .getText().toString();
                lastName = ((EditText) view.findViewById(R.id.input_surname))
                        .getText().toString();

                if (!(email.isEmpty() && password.isEmpty() && name.isEmpty() && lastName.isEmpty())) {

                    if (isValid(email)) {
                        ServiceHelper.signUp(getActivity(), email, password, name, lastName);
                    } else {
                        Snackbar snackbar = Snackbar.make(signUpContainer, "Некорректный e-mail!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
                        params.gravity = Gravity.TOP;
                        view.setLayoutParams(params);
                        snackbar.show();
                    }


                } else {
                    Snackbar snackbar = Snackbar.make(signUpContainer, "Заполните все поля!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    view.setLayoutParams(params);
                    snackbar.show();
                }
            }
        });

    }

    public boolean isValid(String email){
        String regExpr =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }


}
