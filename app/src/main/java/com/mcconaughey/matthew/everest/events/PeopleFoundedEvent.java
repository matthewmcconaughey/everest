package com.mcconaughey.matthew.everest.events;

import com.mcconaughey.matthew.everest.network.REST;

/**
 * Created by dmitri on 22.05.16.
 */
public class PeopleFoundedEvent {
    public REST.Profile[] people;

    public PeopleFoundedEvent(REST.Profile[] people) {
        this.people = people;
    }
}
