package com.mcconaughey.matthew.everest.network.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;

import com.mcconaughey.matthew.everest.database.DataBaseHelper;
import com.mcconaughey.matthew.everest.database.contracts.TaskContract;
import com.mcconaughey.matthew.everest.events.CreateTaskEvent;
import com.mcconaughey.matthew.everest.events.RateTaskEvent;
import com.mcconaughey.matthew.everest.network.ProfileProcessor;
import com.mcconaughey.matthew.everest.network.REST;
import com.mcconaughey.matthew.everest.network.TaskProcessor;
import com.mcconaughey.matthew.everest.utils.CheckPoint;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by dmitri on 28.05.16.
 */
public class TaskIntentService extends IntentService {

    public final static String ACTION_CREATE_TASK = "action.CREATE_TASK";
    public final static String ACTION_GET_FEED = "action.GET_FEED";
    public final static String ACTION_RATE_TASK = "action.RATE_TASK";
    public final static String ACTION_FINISH_CHECKPOINTS = "action.FINISH_CHECKPOINTS";

    public final static String EXTRA_ACCESS_TOKEN = "extra.ACCESS_TOKEN";
    public final static String EXTRA_SERVER_ID = "extra.SERVER_ID";
    public final static String EXTRA_AUTHOR_NAME = "extra.AUTHOR_NAME";
    public final static String EXTRA_SHORT_NAME = "extra.TASK_SHORT_NAME";
    public final static String EXTRA_DESCRIPTION = "extra.DESCRIPTION";
    public final static String EXTRA_CHECKPOINTS = "extra.CHECKPOINTS";
    public final static String EXTRA_RATING = "extra.RATING";
    public final static String EXTRA_ITEM_ID = "extra.ITEM_ID";
    public final static String EXTRA_CREATOR_ID = "extra.CREATOR_ID";
    public final static String EXTRA_SUBSCRIBES = "extra.SUBSCRIBES";
    public final static String EXTRA_TASK_ID = "extra.TASK_ID";
    public final static String EXTRA_FINISHED_CHECKPOINTS = "extra.FINISHED_CHECKPOINTS";

    public TaskIntentService() {
        super("TaskIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_CREATE_TASK:
                    ArrayList<CheckPoint> checkPoints = intent.getParcelableArrayListExtra(EXTRA_CHECKPOINTS);
                    handleCreateTaskAction(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_AUTHOR_NAME), intent.getStringExtra(EXTRA_SERVER_ID),
                            intent.getStringExtra(EXTRA_SHORT_NAME), intent.getStringExtra(EXTRA_DESCRIPTION),
                            checkPoints);
                    break;
                case ACTION_GET_FEED:
                    handleGetFeed(intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringArrayListExtra(EXTRA_SUBSCRIBES));
                    break;
                case ACTION_RATE_TASK:
                    handleRateTask(intent.getStringExtra(EXTRA_CREATOR_ID),
                            intent.getStringExtra(EXTRA_ITEM_ID), intent.getStringExtra(EXTRA_SERVER_ID),
                            intent.getStringExtra(EXTRA_ACCESS_TOKEN));
                case ACTION_FINISH_CHECKPOINTS:
                    handleFinishCheckpoints(
                            intent.getStringExtra(EXTRA_ACCESS_TOKEN),
                            intent.getStringExtra(EXTRA_TASK_ID),
                            intent.getIntegerArrayListExtra(EXTRA_FINISHED_CHECKPOINTS)
                    );
                    break;
            }
        }
    }

    private void handleRateTask(String creatorId, String itemId, String serverId, String accessToken){

        if (creatorId != null && itemId != null && serverId != null && accessToken != null){
            RateTaskEvent event = TaskProcessor.processLikeTask(getContentResolver(), creatorId, serverId, accessToken);
            event.itemId = serverId;
            EventBus.getDefault().post(event);
        }

    }

    private void handleCreateTaskAction(String accessToken, String authorName, String serverID, String shortName,
                                        String description, ArrayList<CheckPoint> checkPoints) {
        if (accessToken != null && shortName != null && description != null && checkPoints != null) {
            boolean state = TaskProcessor.processCreateTask(getContentResolver(), accessToken, authorName, serverID, shortName,
                    description, checkPoints);
            EventBus.getDefault().post(new CreateTaskEvent(state));
        }
    }

    private void handleGetFeed(String accessToken, ArrayList<String> subscribes) {
        if (accessToken != null && subscribes != null) {
            ArrayList<REST.Task> feed = TaskProcessor.processGetFeed(accessToken, subscribes);
            if (feed != null) {
                if (!feed.isEmpty()) {
                    DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
                    dataBaseHelper.clearTasksAndCheckpoints();
                }

                ContentValues[] taskValues = new ContentValues[feed.size()];
                ArrayList<CheckPoint> checkPoints = new ArrayList<>();
                int i = 0;
                for (REST.Task task : feed) {
                    taskValues[i] = new ContentValues();
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_TASK_ID, task.taskID);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_SERVER_ID, task.serverID);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_AUTHOR_NAME, task.authorName);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_SHORT_NAME, task.shortName);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_DESCRIPTION, task.description);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_CREATION_TIME, task.creationTime);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_RATING, task.rating);
                    taskValues[i].put(TaskContract.Tasks.COLUMN_NAME_RATED, task.rated);
                    setTaskIdToCheckpoints(task.undoneCheckpoints, task.taskID);
                    checkPoints.addAll(task.undoneCheckpoints);
                    i++;
                }
                getContentResolver().bulkInsert(TaskContract.Tasks.CONTENT_URI, taskValues);
                if (feed.isEmpty())
                    getContentResolver().notifyChange(TaskContract.Tasks.CONTENT_URI, null);

                ContentValues[] checkpointValues = new ContentValues[checkPoints.size()];
                i = 0;
                for (CheckPoint checkPoint : checkPoints) {
                    checkpointValues[i] = new ContentValues();
                    checkpointValues[i].put(TaskContract.Checkpoints.COLUMN_NAME_TASK_ID, checkPoint.taskID);
                    checkpointValues[i].put(TaskContract.Checkpoints.COLUMN_NAME_TEXT, checkPoint.text);
                    checkpointValues[i].put(TaskContract.Checkpoints.COLUMN_NAME_ID, checkPoint.id);
                    checkpointValues[i].put(TaskContract.Checkpoints.COLUMN_NAME_STATE, checkPoint.state);
                    i++;
                }
                getContentResolver().bulkInsert(TaskContract.Checkpoints.CONTENT_URI, checkpointValues);
            }
        }
    }

    private void setTaskIdToCheckpoints(ArrayList<CheckPoint> checkpoints, String taskID) {
        for (CheckPoint checkPoint : checkpoints) {
            checkPoint.taskID = taskID;
        }
    }

    private void handleFinishCheckpoints(String accessToken, String taskID,
                                         ArrayList<Integer> finishedCheckPoints) {
        if (accessToken != null && taskID != null && finishedCheckPoints != null) {
            if (TaskProcessor.processFinishTask(accessToken, taskID, finishedCheckPoints)) {
                getContentResolver();
            }
        }
    }
}
