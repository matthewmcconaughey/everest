package com.mcconaughey.matthew.everest.events;

/**
 * Created by alexander on 16.04.16.
 */
public class SignInEvent {
    public String accessToken;

    public SignInEvent(String accessToken) {
        this.accessToken = accessToken;
    }

}
