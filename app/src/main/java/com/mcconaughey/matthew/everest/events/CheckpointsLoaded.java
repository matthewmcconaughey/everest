package com.mcconaughey.matthew.everest.events;

import android.database.Cursor;

/**
 * Created by dmitri on 29.05.16.
 */
public class CheckpointsLoaded {
    public Cursor checkpoints;

    public CheckpointsLoaded(Cursor checkpoints) {
        this.checkpoints = checkpoints;
    }
}
