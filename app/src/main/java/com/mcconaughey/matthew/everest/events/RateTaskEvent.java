package com.mcconaughey.matthew.everest.events;

/**
 * Created by alexander on 30.05.16.
 */
public class RateTaskEvent {
    public boolean state;
    public String resp;
    public String rating;
    public String itemId;

    public RateTaskEvent(boolean state, String resp, String rating, String itemId) {
        this.state = state;
        this.rating = rating;
        this.resp = resp;
        this.itemId = itemId;
    }
}
