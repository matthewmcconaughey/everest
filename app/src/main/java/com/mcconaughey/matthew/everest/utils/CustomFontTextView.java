package com.mcconaughey.matthew.everest.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by alexander on 13.04.16.
 */
public class CustomFontTextView extends TextView {

    public CustomFontTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/SketchRockwell-Bold.ttf"));
    }
}
