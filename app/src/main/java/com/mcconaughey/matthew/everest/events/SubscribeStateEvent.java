package com.mcconaughey.matthew.everest.events;

/**
 * Created by denis on 23.05.16.
 */
public class SubscribeStateEvent {
    public boolean state;

    public SubscribeStateEvent(boolean state) {
        this.state = state;
    }

}
